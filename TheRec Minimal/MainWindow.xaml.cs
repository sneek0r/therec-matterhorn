﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using TheRec_Controller;
using DirectShowLib;

namespace TheRec_Minimal
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Dictionary<String, DsDevice> AudioDevices;
        Dictionary<String, DsDevice> VideoDevices;

        ITheRecGraph2 Graph = null;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateDevicesMenu();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            StopGraph();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (sender == QuitMenuItem)
            {
                Shutdown();
            }
            else if (sender == UpdateDevicesMenuItem)
            {
                UpdateDevicesMenu();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (sender == PreviewButton)
            {
                if (Graph != null) StopGraph();
                else RunPreviewGraph();
            }
            else if (sender == CaptureButton)
            {
                if (Graph != null) StopGraph();
                else RunCaptureGraph();
            }
        }

        protected void Shutdown()
        {
            StopGraph();
            Application.Current.Shutdown();
        }

        protected void UpdateDevicesMenu()
        {
            DevicesMenu.Items.Clear();

            AudioDevices = DsDevice.GetDevicesOfCat(FilterCategory.AudioInputDevice).ToDictionary(dev => dev.Name);

            foreach (String Name in AudioDevices.Keys)
            {
                MenuItem DeviceMenuItem = new MenuItem();
                DeviceMenuItem.Header = Name;
                DeviceMenuItem.IsCheckable = true;
                DevicesMenu.Items.Add(DeviceMenuItem);
            }

            DevicesMenu.Items.Add(new Separator());

            VideoDevices = DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice).ToDictionary(dev => dev.Name);

            foreach (String Name in VideoDevices.Keys)
            {
                MenuItem DeviceMenuItem = new MenuItem();
                DeviceMenuItem.Header = Name;
                DeviceMenuItem.IsCheckable = true;
                DevicesMenu.Items.Add(DeviceMenuItem);
            }
        }

        void CreatePreview(List<String> VideoDeviceNames, ITheRecGraph2 Graph)
        {
            RemovePreview();
            foreach (String DeviceName in VideoDeviceNames)
            {
                DevicePreviewFrame PreviewFrame = new DevicePreviewFrame(DeviceName, Graph);
                VideoPreviewGrid.Children.Add(PreviewFrame);

                VideoPreviewGrid.Rows = VideoPreviewGrid.Children.Count > 2 ? 2 : 1;
                VideoPreviewGrid.Columns = VideoPreviewGrid.Rows == 1 ?
                    VideoPreviewGrid.Children.Count :
                    VideoPreviewGrid.Children.Count / VideoPreviewGrid.Rows + VideoPreviewGrid.Children.Count % VideoPreviewGrid.Rows;

                PreviewFrame.UpdateGraphSettings();
            }
        }

        void RemovePreview()
        {
            VideoPreviewGrid.Children.Clear();
            VideoPreviewGrid.Columns = 1;
            VideoPreviewGrid.Rows = 1;
        }

        void RunPreviewGraph()
        {
            StopGraph();

            List<DsDevice> Devices = GetCheckedDevices();
            Graph = new TheRec_Controller.TheRecPreviewGraph2();

            foreach (DsDevice Device in Devices)
            {
                if (VideoDevices.ContainsValue(Device))
                {
                    try
                    {
                        Graph.AddSource(Device);
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.StackTrace);
                    }
                }
            }

            Graph.Run();

            CreatePreview(Devices.Where(dev => VideoDevices.ContainsValue(dev)).Select(dev => dev.Name).ToList<String>(), Graph);
        }

        void RunCaptureGraph()
        {
            StopGraph();

            List<DsDevice> Devices = GetCheckedDevices();
            Graph = new TheRec_Controller.TheRecCaptureMPEGGraph2(Environment.GetFolderPath(Environment.SpecialFolder.MyVideos));

            foreach (DsDevice Device in Devices)
            {
                try
                {
                    Graph.AddSource(Device);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.StackTrace);
                }
            }

            Graph.Run();

            CreatePreview(Devices.Where(dev => VideoDevices.ContainsValue(dev)).Select(dev => dev.Name).ToList<String>(), Graph);
        }

        void StopGraph()
        {
            if (Graph != null)
            {
                Graph.Stop();
                Graph.Dispose();
                Graph = null;
            }
        }

        List<DsDevice> GetCheckedDevices()
        {
            List<DsDevice> CheckedDevices = new List<DsDevice>();

            foreach (object Item in DevicesMenu.Items)
            {
                if (Item is MenuItem)
                {
                    MenuItem MItem = (MenuItem)Item;
                    if (MItem.IsCheckable && MItem.IsChecked)
                    {
                        DsDevice CheckedDevice = null;
                        if (AudioDevices.ContainsKey((string)MItem.Header))
                        {
                            CheckedDevice = AudioDevices[(string)MItem.Header];
                        }
                        else if (VideoDevices.ContainsKey((string)MItem.Header))
                        {
                            CheckedDevice = VideoDevices[(string)MItem.Header];
                        }

                        if (CheckedDevice != null) CheckedDevices.Add(CheckedDevice);
                    }
                }
            }

            return CheckedDevices;
        }
    }
}
