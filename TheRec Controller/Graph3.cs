﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DirectShowLib;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using System.Threading;

namespace TheRec_Controller
{
    public abstract class TheRecGraph3 : ITheRecGraph3
    {
        public static readonly int DEFAULT_FRAMERATE = 25;

        protected ICaptureGraphBuilder2 GraphBuilder = new CaptureGraphBuilder2() as ICaptureGraphBuilder2;
        protected IGraphBuilder Graph = new FilterGraph() as IGraphBuilder;

#if DEBUG
        private DsROTEntry ROTEntry;
#endif

        protected DsDevice Device;
        protected float ImageAspectRatio;

        public TheRecGraph3()
        {
            int hr = GraphBuilder.SetFiltergraph(Graph);
            DsError.ThrowExceptionForHR(hr);

#if DEBUG
            ROTEntry = new DsROTEntry(Graph);
#endif
        }

        protected void ReleaseInterfaces()
        {
            Debug.WriteLine("Dispose Graph for " + Device != null? Device.Name : "?");

            if (EventsT != null && !EventsT.Join(5000))
            {
                _EventsTShouldStop = true;
                EventsT.Join();
            }
                

            if (GetFilterState() != FilterState.Stopped)
                Stop();

            if (Graph != null)
            {
                Marshal.ReleaseComObject(Graph);
                Graph = null;
            }

            if (GraphBuilder != null)
            {
                Marshal.ReleaseComObject(GraphBuilder);
                GraphBuilder = null;
            }

#if DEBUG
            if (ROTEntry != null)
            {
                ROTEntry.Dispose();
                ROTEntry = null;
            }
#endif
        }

        public void Dispose()
        {
            ReleaseInterfaces();
            GC.SuppressFinalize(this);
        }

        public virtual void Pause()
        {
            int hr;
            IMediaControl control = Graph as IMediaControl;
            if (control != null)
            {
                string OldState = GetState();

                TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingEventArgs(
                    Device.DevicePath, OldState, FilterStateToString(FilterState.Paused)));

                try
                {
                    hr = control.Pause();
                    DsError.ThrowExceptionForHR(hr);

                    TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangedEventArgs(
                        Device.DevicePath, OldState, FilterStateToString(FilterState.Paused)));
                }
                catch (Exception e)
                {
                    TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingFailedEventArgs(
                        Device.DevicePath, OldState, FilterStateToString(FilterState.Paused)));
                    throw e;
                }
            }
        }

        Thread EventsT = null;
        public virtual void Run()
        {
            int hr;
            IMediaControl control = Graph as IMediaControl;
            if (control != null)
            {
                string OldState = GetState();

                TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingEventArgs(
                    Device.DevicePath, OldState, FilterStateToString(FilterState.Running)));

                try
                {
                    EventsT = new Thread(HandleGraphEvent);
                    EventsT.Start();

                    hr = control.Run();
                    DsError.ThrowExceptionForHR(hr);

                    TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangedEventArgs(
                        Device.DevicePath, OldState, FilterStateToString(FilterState.Running)));
                }
                catch (Exception e)
                {
                    TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingFailedEventArgs(
                        Device.DevicePath, OldState, FilterStateToString(FilterState.Running)));
                    throw e;
                }
            }
        }

        public virtual void Stop()
        {
            int hr;
            IMediaControl control = Graph as IMediaControl;
            if (control != null)
            {
                string OldState = GetState();

                TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingEventArgs(
                    Device.DevicePath, OldState, FilterStateToString(FilterState.Stopped)));
                
                try
                {
                    hr = control.StopWhenReady();
                    //hr = control.Stop();
                    DsError.ThrowExceptionForHR(hr);
                    
                    TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangedEventArgs(
                        Device.DevicePath, OldState, FilterStateToString(FilterState.Stopped)));
                }
                catch (Exception e)
                {
                    TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingFailedEventArgs(
                        Device.DevicePath, OldState, FilterStateToString(FilterState.Stopped)));
                    throw e;
                }
            }
        }

        public string GetState()
        {
            return FilterStateToString(GetFilterState());
        }

        protected virtual FilterState GetFilterState()
        {
            int hr;
            IMediaControl control = Graph as IMediaControl;
            if (control != null)
            {
                FilterState State;
                hr = control.GetState(int.MaxValue, out State);
                DsError.ThrowExceptionForHR(hr);
                return State;
            }
            return FilterState.Stopped;
        }

        public static string FilterStateToString(FilterState State)
        {
            switch (State)
            {
                case FilterState.Paused: return "paused";
                case FilterState.Running: return "running";
                case FilterState.Stopped: return "stopped";
                default: return "stopped";
            }
        }

        public virtual double GetDuration()
        {
            int hr;
            IMediaPosition position = Graph as IMediaPosition;
            if (position != null)
            {
                double duration;
                hr = position.get_CurrentPosition(out duration);
                DsError.ThrowExceptionForHR(hr);
                return duration;
            }
            return 0.0d;
        }

        private volatile bool _EventsTShouldStop = false;
        public virtual void HandleGraphEvent()
        {
            int hr;
            IMediaEvent me = Graph as IMediaEvent;
            if (me == null) return;
            hr = me.CancelDefaultHandling(EventCode.StateChange);
            DsError.ThrowExceptionForHR(hr);
            hr = me.CancelDefaultHandling(EventCode.Complete);
            DsError.ThrowExceptionForHR(hr);

            EventCode ev;
            IntPtr param1, param2;

            while (!_EventsTShouldStop)
            {
                hr = me.GetEvent(out ev, out param1, out param2, 1000);
                if (hr >= 0)
                {
                    Debug.WriteLine("{0} {2} event: {1}", DateTime.Now, ev, Device.Name);
                    switch (ev)
                    {
                        case EventCode.Complete:
                        case EventCode.ErrorAbort:
                            // error!!! // no more harddisk space?
                            TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphErrorEventArgs(Device.DevicePath));
                            _EventsTShouldStop = true;
                            break;
                        case EventCode.UserAbort:
                        //case EventCode.DeviceLost:
                            break;
                        case EventCode.DisplayChanged:
                            break;
                        case EventCode.StateChange:
                            FilterState State = (FilterState)param1.ToInt32();
                            Debug.WriteLine("{0} state change to {1}", Device.Name, State);
                            if (State == FilterState.Stopped) 
                                _EventsTShouldStop = true;
                            break;
                        case EventCode.Paused:
                            hr = param1.ToInt32();
                            Debug.WriteLine("{0} was paused with {1:x}!", Device.Name, hr);
                            break;

                        default: break;
                    }
                    me.FreeEventParams(ev, param1, param2);
                }
            }
        }

        public virtual void UpdateVideoWindowPosition(IntPtr VideoWindowOwner, int width, int height)
        {
            int hr;
            IVideoWindow window = Graph as IVideoWindow;
            if (window != null)
            {
                hr = window.put_Owner(VideoWindowOwner);
                DsError.ThrowExceptionForHR(hr);

                if (VideoWindowOwner != IntPtr.Zero)
                {
                    hr = window.put_WindowStyle(WindowStyle.Child | WindowStyle.ClipSiblings);
                    DsError.ThrowExceptionForHR(hr);

                    hr = window.put_Visible(OABool.True);
                    DsError.ThrowExceptionForHR(hr);
                }
                else
                {
                    hr = window.put_Visible(OABool.False);
                    DsError.ThrowExceptionForHR(hr);
                }

                int posX = 0, posY = 0;
                if (ImageAspectRatio > 0)
                {
                    int widthN = Math.Min(width, (int)(height * ImageAspectRatio));
                    int heightN = Math.Min(height, (int)(width / ImageAspectRatio));
                    posX = (width - widthN) / 2;   // Center within control
                    posY = (height - heightN) / 2;
                    width = widthN; height = heightN;
                }

                hr = window.SetWindowPosition(posX, posY, width, height);
                DsError.ThrowExceptionForHR(hr);
            }
        }

        protected void TrySetVideoParams(IBaseFilter SourceFilter, int FrameRate)
        {
            int hr;
            object o;
            AMMediaType media = null;
            IAMStreamConfig videoStreamConfig = null;
            IAMVideoControl videoControl = SourceFilter as IAMVideoControl;

            // Find the stream config interface
            hr = GraphBuilder.FindInterface(null, MediaType.Video, SourceFilter, typeof(IAMStreamConfig).GUID, out o);

            videoStreamConfig = o as IAMStreamConfig;
            try
            {
                if (videoStreamConfig == null)
                {
                    ImageAspectRatio = 4f / 3f;
                    return;
                }

                hr = videoStreamConfig.GetFormat(out media);
                DsError.ThrowExceptionForHR(hr);

                if (media != null)
                {
                    if (media.formatType == FormatType.VideoInfo)
                    {
                        // copy out the videoinfoheader
                        VideoInfoHeader v = new VideoInfoHeader();
                        Marshal.PtrToStructure(media.formatPtr, v);

                        // if overriding the framerate, set the frame rate
                        if (FrameRate > 0)
                        {
                            v.AvgTimePerFrame = 10000000 / FrameRate;

                            // Copy the media structure back
                            Marshal.StructureToPtr(v, media.formatPtr, true);

                            // Set the new format
                            hr = videoStreamConfig.SetFormat(media);
                            DsError.ThrowExceptionForHR(hr);

                            DsUtils.FreeAMMediaType(media);
                            media = null;

                            hr = videoStreamConfig.GetFormat(out media);
                            DsError.ThrowExceptionForHR(hr);

                            Marshal.PtrToStructure(media.formatPtr, v);
                        }
                        
                        // Format successfully set, calculate image aspect ratio
                        ImageAspectRatio = Math.Abs((float)v.BmiHeader.Width / (float)v.BmiHeader.Height);
                    }
                    else if (media.formatType == FormatType.VideoInfo2)
                    {
                        VideoInfoHeader2 v = new VideoInfoHeader2();
                        Marshal.PtrToStructure(media.formatPtr, v);

                        if (v != null && v.BmiHeader.Height > 0 && v.BmiHeader.Width > 0)
                        {
                            ImageAspectRatio = Math.Abs((float)v.BmiHeader.Height / (float)v.BmiHeader.Width);
                        }
                        Marshal.ReleaseComObject(v);
                    }
                }

                //// Fix upsidedown video
                //if (videoControl != null)
                //{
                //    VideoControlFlags pCapsFlags;

                //    IPin pPin = DsFindPin.ByCategory(SourceFilter, PinCategory.Capture, 0);
                //    hr = videoControl.GetCaps(pPin, out pCapsFlags);
                //    DsError.ThrowExceptionForHR(hr);

                //    if ((pCapsFlags & VideoControlFlags.FlipVertical) > 0)
                //    {
                //        hr = videoControl.GetMode(pPin, out pCapsFlags);
                //        DsError.ThrowExceptionForHR(hr);

                //        hr = videoControl.SetMode(pPin, 0);
                //    }
                //}
            }
            finally
            {
                if (videoStreamConfig != null)
                    Marshal.ReleaseComObject(videoStreamConfig);

                if (media != null)
                    DsUtils.FreeAMMediaType(media);
            }
        }

        protected bool SourceDeviceAccept(IBaseFilter SourceFilter, Guid MediaType)
        {
            int hr;
            IPin pin;
            for (int index = 0; index < 10; index++)
            {
                hr = GraphBuilder.FindPin(SourceFilter, PinDirection.Output, DsGuid.Empty, MediaType, false, index, out pin);
                if (pin != null)
                {
                    Marshal.ReleaseComObject(pin);
                    return true;
                }
                else if (hr == 0) return true;
            }
            return false;
        }

        public static DateTime DurationToDateTime(double millisec)
        {
            //return new DateTime((long)((int)millisec * TimeSpan.TicksPerMillisecond)); // convert milli sec to nano sec
            int _millisec = (int)millisec;
            int h = _millisec / 3600;
            int m = (_millisec - (h * 3600)) / 60;
            int s = _millisec - (h * 3600 + m * 60);

            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, h, m, s);
        }

        public abstract bool IsCaptureGraph();
        public abstract void AddSource(string DevicePath, string FriendlyName, string Flavour, string OutputPath);
    }

    public class TheRecPreviewGraph3 : TheRecGraph3
    {
        public override bool IsCaptureGraph()
        {
            return false;
        }

        public override void AddSource(string DevicePath, string FriendlyName, string Ignore, string Ignore2)
        {
            int hr;
            Device = Utils.GetDsDeviceOrNull(DevicePath);
            if (Device == null) throw new Exception("Device not exist!");

            IBaseFilter SourceFilter;
            hr = ((IFilterGraph2)Graph).AddSourceFilterForMoniker(Device.Mon, null, Device.Name, out SourceFilter);
            DsError.ThrowExceptionForHR(hr);

            //IBaseFilter VRenderer = new VideoRendererDefault() as IBaseFilter;
            //hr = Graph.AddFilter(VRenderer, "Video Renderer");
            //DsError.ThrowExceptionForHR(hr);

            //hr = GraphBuilder.RenderStream(PinCategory.Preview, MediaType.Video, SourceFilter, null, VRenderer);
            hr = GraphBuilder.RenderStream(PinCategory.Preview, MediaType.Video, SourceFilter, null, null);

            if (hr >= 0)
            {
                //IVideoWindow VideoWindow = VRenderer as IVideoWindow;
                IVideoWindow VideoWindow = Graph as IVideoWindow;
                hr = VideoWindow.put_AutoShow(OABool.False);
                DsError.ThrowExceptionForHR(hr);
                TrySetVideoParams(SourceFilter, DEFAULT_FRAMERATE);
            }
            else
            {
                // preview wo audio!!!
                DsError.ThrowExceptionForHR(hr);
            }

            //Marshal.ReleaseComObject(VRenderer);
            Marshal.ReleaseComObject(SourceFilter);
        }
    }

    public class TheRecCaptureMPEGGraph3 : TheRecGraph3
    {
        public static readonly Guid MPEG_ENCODER_GUID = new Guid(@"{5F5AFF4A-2F7F-4279-88C2-CD88EB39D144}");

        public override bool IsCaptureGraph()
        {
            return true;
        }

        public override void AddSource(string DevicePath, string FriendlyName, string Flavour, string OutputPath)
        {
            int hr;
            Device = Utils.GetDsDeviceOrNull(DevicePath);
            if (Device == null) throw new Exception("Device not exist!");

            if (string.IsNullOrEmpty(OutputPath))
                throw new Exception("Output path not set!");

            if (string.IsNullOrEmpty(FriendlyName))
                FriendlyName = Device.Name;

            if (!Directory.Exists(OutputPath))
                Directory.CreateDirectory(OutputPath);

            if (File.Exists(Path.Combine(OutputPath, String.Format("{0}{1}.mpg", FriendlyName, Flavour))))
                throw new Exception("Outputfile is allready exist!");

            IBaseFilter SourceFilter;
            hr = ((IFilterGraph2)Graph).AddSourceFilterForMoniker(Device.Mon, null, Device.Name, out SourceFilter);
            DsError.ThrowExceptionForHR(hr);

            Type type = Type.GetTypeFromCLSID(MPEG_ENCODER_GUID);
            IBaseFilter Encoder = (IBaseFilter)Activator.CreateInstance(type);

            if (Encoder == null)
                throw new Exception("MPEG encoder not found! Do you running Windows 7?");

            hr = Graph.AddFilter(Encoder, "MPEG Encoder");
            DsError.ThrowExceptionForHR(hr);

            //string FileNameTmp = Path.Combine(OutputPath, FriendlyName + ".mpg");
            //int FileNamePostfix = 0;
            //while (File.Exists(FileNameTmp))
            //{
            //    FileNamePostfix++;
            //    FileNameTmp = Path.Combine(OutputPath, FriendlyName + "_" + FileNamePostfix + ".mpg");
            //}

            IFileSinkFilter2 FileSink = new FileWriter() as IFileSinkFilter2;
            //FileSink.SetFileName(FileNameTmp, null);
            FileSink.SetFileName(Path.Combine(OutputPath, String.Format("{0}{1}.mpg", FriendlyName, Flavour)), null);
            FileSink.SetMode(AMFileSinkFlags.None);
            Graph.AddFilter(FileSink as IBaseFilter, "File Sink");

            hr = GraphBuilder.RenderStream(null, null, Encoder, null, FileSink as IBaseFilter);
            DsError.ThrowExceptionForHR(hr);

            try
            {
                LinkDVSource(SourceFilter, Encoder);
            }
            catch (Exception)
            {
                bool linked = false;
                try
                {
                    LinkAudioSource(SourceFilter, Encoder);
                    linked = true;
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.StackTrace);
                }

                try
                {
                    LinkVideoSource(SourceFilter, Encoder);
                    linked = true;
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.StackTrace);
                }

                if (!linked) throw new Exception("Can not determine media type!");
            }
        }

        protected void LinkDVSource(IBaseFilter SourceFilter, IBaseFilter Encoder)
        {
            int hr;
            IBaseFilter DVSplitter = new DVSplitter() as IBaseFilter;
            hr = Graph.AddFilter(DVSplitter, "DV Splitter");
            DsError.ThrowExceptionForHR(hr);

            hr = GraphBuilder.RenderStream(PinCategory.Capture, MediaType.Interleaved, SourceFilter, DVSplitter, Encoder);
            if (hr < 0)
            {
                Graph.RemoveFilter(DVSplitter);
                DsError.ThrowExceptionForHR(hr);
            }

            hr = GraphBuilder.RenderStream(null, null, DVSplitter, null, Encoder);
            if (hr < 0)
            {
                Graph.RemoveFilter(DVSplitter);
                DsError.ThrowExceptionForHR(hr);
            }

            //IBaseFilter VideoRenderer = new VideoRendererDefault() as IBaseFilter;
            //hr = Graph.AddFilter(VideoRenderer, "Video Renderer");

            //hr = GraphBuilder.RenderStream(PinCategory.Preview, MediaType.Interleaved, SourceFilter, null, VideoRenderer);
            hr = GraphBuilder.RenderStream(PinCategory.Preview, MediaType.Interleaved, SourceFilter, null, null);
            if (hr < 0)
            {
                Graph.RemoveFilter(DVSplitter);
                //Graph.RemoveFilter(VideoRenderer);
                DsError.ThrowExceptionForHR(hr);
            }
            else
            {
                //IVideoWindow VideoWindow = VideoRenderer as IVideoWindow;
                IVideoWindow VideoWindow = Graph as IVideoWindow;
                hr = VideoWindow.put_AutoShow(OABool.False);
                TrySetVideoParams(SourceFilter, -1);

                IBasicAudio AudioControl = Graph as IBasicAudio;
                if (AudioControl != null)
                {
                    hr = AudioControl.put_Volume(-10000); // -100 dB
                    DsError.ThrowExceptionForHR(hr);
                }
            }
        }

        protected void LinkAudioSource(IBaseFilter SourceFilter, IBaseFilter Encoder)
        {
            int hr;
            hr = GraphBuilder.RenderStream(null, MediaType.Audio, SourceFilter, null, Encoder);
            DsError.ThrowExceptionForHR(hr);
        }

        protected void LinkVideoSource(IBaseFilter SourceFilter, IBaseFilter Encoder)
        {
            int hr;
            //IDMOWrapperFilter DMOFrameRateConverter = new DMOWrapperFilter() as IDMOWrapperFilter;
            //hr = DMOFrameRateConverter.Init(new Guid("{01F36CE2-0907-4D8B-979D-F151BE91C883}"), DMOCategory.VideoEffect);
            //DMOError.ThrowExceptionForHR(hr);
            //hr = Graph.AddFilter(DMOFrameRateConverter as IBaseFilter, "Framerate Converter");
            //DMOError.ThrowExceptionForHR(hr);

            //hr = GraphBuilder.RenderStream(PinCategory.Capture, MediaType.Video, SourceFilter, null, DMOFrameRateConverter as IBaseFilter);
            //DsError.ThrowExceptionForHR(hr);
            //// setup framerate on DMO
            ////IPropertyStore propstore = DMOFrameRateConverter as IPropertyStore;
            ////if (propstore != null)
            ////{
            ////    uint Count;
            ////    if (propstore.GetCount(out Count) >= 0)
            ////    {
            ////        Debug.WriteLine("DMO has {0} properties", Count);

            ////        for (uint i = 0; i < Count; i++)
            ////        {
            ////            PROPERTYKEY propkey;
            ////            if (propstore.GetAt(i, out propkey) >= 0)
            ////            {
            ////                Debug.WriteLine("propkey.fmtid: {0}, propkey.pid: {1}", new Object[] { propkey.fmtid, propkey.pid.ToString() });

            ////                PropVariant propv;
            ////                if (propstore.GetValue(ref propkey, out propv) >= 0)
            ////                {
            ////                    Debug.WriteLine("propv.type: {0}, propv.value: {1}/{2}", propv.variantType, propv.value.numerator, propv.value.denumerator);
            ////                    if (propkey.pid.ToUInt32() == 2 && propv.variantType == 21)
            ////                    {
            ////                        propv.value.numerator = 25u;
            ////                        propv.value.denumerator = 1u;

            ////                        if (propstore.SetValue(ref propkey, ref propv) >= 0)
            ////                        {
            ////                            Debug.WriteLine("juhu");

            ////                            propstore.GetValue(ref propkey, out propv);
            ////                            Debug.WriteLine("propv.type: {0}, propv.value: {1}/{2}", propv.variantType, propv.value.numerator, propv.value.denumerator);
            ////                        }
            ////                    }
            ////                }
            ////            }
            ////        }
            ////    }
            ////}
            //hr = GraphBuilder.RenderStream(PinCategory.Capture, MediaType.Video, SourceFilter, DMOFrameRateConverter as IBaseFilter, Encoder);

            hr = GraphBuilder.RenderStream(PinCategory.Capture, MediaType.Video, SourceFilter, null, Encoder);
            //if (hr < 0)
            //{
            //    Graph.RemoveFilter(DMOFrameRateConverter as IBaseFilter);
            DsError.ThrowExceptionForHR(hr);
            //}

            //IBaseFilter VideoRenderer = new VideoRendererDefault() as IBaseFilter;
            //hr = Graph.AddFilter(VideoRenderer, "Video Renderer");
            //DsError.ThrowExceptionForHR(hr);

            //hr = GraphBuilder.RenderStream(PinCategory.Preview, MediaType.Video, SourceFilter, null, VideoRenderer);
            hr = GraphBuilder.RenderStream(PinCategory.Preview, MediaType.Video, SourceFilter, null, null);
            if (hr < 0)
            {
                //Graph.RemoveFilter(VideoRenderer);
                DsError.ThrowExceptionForHR(hr);
            }
            else
            {
                //IVideoWindow VideoWindow = VideoRenderer as IVideoWindow;
                IVideoWindow VideoWindow = Graph as IVideoWindow;
                hr = VideoWindow.put_AutoShow(OABool.False);
                TrySetVideoParams(SourceFilter, DEFAULT_FRAMERATE);
            }
        }
    }
}
