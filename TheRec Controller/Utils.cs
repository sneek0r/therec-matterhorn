﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DirectShowLib;

namespace TheRec_Controller
{
    public class Utils
    {
        public static DsDevice GetDsDeviceOrNull(string Path)
        {
            List<DsDevice> Devices = DsDevice.GetDevicesOfCat(FilterCategory.AudioInputDevice).ToList();
            Devices.AddRange(DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice).ToList());
            foreach (DsDevice Device in Devices.Where(dev => dev.DevicePath == Path))
            {
                return Device;
            }
            return null;
        }
    }
}
