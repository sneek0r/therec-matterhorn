﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DirectShowLib;
using DirectShowLib.DMO;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;

namespace TheRec_Controller
{
    /// <summary>
    /// Type of stream from capture device
    /// </summary>
    public enum DeviceTypeEnum
    {
        Audio,          // Audio only
        Video,          // Video only
        AV,             // Audio and Video
        DV,             // Digital Video camera
        Unspecified     // unknown type
    }

    public abstract class TheRecGraph : IDisposable, ITheRecGraph
    {
        public const int WM_GRAPHNOTIFY = 0x8000 + 1;
        protected const int DEFAULT_FRAMERATE = 25;

        private bool Disposing = false;

        public readonly DsDevice Device;
        public DeviceTypeEnum DeviceType { get { return _DeviceType; } }
        protected DeviceTypeEnum _DeviceType = DeviceTypeEnum.Unspecified;

        protected IGraphBuilder Graph = new FilterGraph() as IGraphBuilder;
        protected ICaptureGraphBuilder2 GraphBuilder = new CaptureGraphBuilder2() as ICaptureGraphBuilder2;

#if DEBUG
        private DsROTEntry ROTEntry;
#endif

        protected IBaseFilter SourceFilter = null;

        protected IMediaControl MediaControl { get { return Graph as IMediaControl; } }
        protected IMediaEvent MediaEvent { get { return Graph as IMediaEvent; } }
        protected IMediaEventEx MediaEventEx { get { return Graph as IMediaEventEx; } }
        //protected IVideoWindow VideoWindow { get { return Graph as IVideoWindow; } }
        protected IVideoWindow VideoWindow  = null;
        protected IMediaPosition MediaPosition { get { return Graph as IMediaPosition; } }

        protected IntPtr MessageHandler = IntPtr.Zero;

        protected float ImageAspectRatio = 4.0f / 3.0f;

        protected IntPtr VideoWindowOwner = IntPtr.Zero;

        public TheRecGraph(DsDevice Device)
        {
            this.Device = Device;

            SetupInterfaces();
            //BuildGraph();

            //if (MessageHandler != IntPtr.Zero)
            //{
            //    int hr = MediaEventEx.SetNotifyWindow(MessageHandler, WM_GRAPHNOTIFY, IntPtr.Zero);
            //    DsError.ThrowExceptionForHR(hr);
            //}
        }

        protected void SetupInterfaces()
        {
            int hr = GraphBuilder.SetFiltergraph(Graph);
            DsError.ThrowExceptionForHR(hr);

#if DEBUG
            ROTEntry = new DsROTEntry(Graph);
#endif

            hr = ((IFilterGraph2)Graph).AddSourceFilterForMoniker(Device.Mon, null, Device.Name, out SourceFilter);
            DsError.ThrowExceptionForHR(hr);
        }

        protected virtual void ReleaseInterfaces()
        {
            if (this.Disposing) return;
            this.Disposing = true;

            Stop();

            if (Graph != null)
            {
                Debug.WriteLine(Device.Name + " " + this.GetType().Name);
                Marshal.ReleaseComObject(Graph);
                Graph = null;
            }

            if (GraphBuilder != null)
            {
                Marshal.ReleaseComObject(GraphBuilder);
                Graph = null;
            }

            if (SourceFilter != null)
            {
                Marshal.ReleaseComObject(SourceFilter);
                SourceFilter = null;
            }

#if DEBUG
            if (ROTEntry != null)
            {
                ROTEntry.Dispose();
                ROTEntry = null;
            }
#endif

        }

        public virtual void Dispose()
        {
            ReleaseInterfaces();
            GC.SuppressFinalize(this);
        }

        public virtual void Run()
        {
            if (MediaControl != null)
            {
                FilterState OldState = GetFilterState();

                TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingEventArgs(
                    Device.DevicePath, FilterStateToString(OldState), FilterStateToString(FilterState.Running)));

                int hr = MediaControl.Run();
                try
                {
                    DsError.ThrowExceptionForHR(hr);
                    TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangedEventArgs(
                        Device.DevicePath, FilterStateToString(OldState), FilterStateToString(FilterState.Running)));
                }
                catch (Exception e)
                {
                    TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingFailedEventArgs(
                        Device.DevicePath, FilterStateToString(OldState), FilterStateToString(FilterState.Running)));
                    throw e;
                }
            }
        }
        
        public virtual void Stop()
        {
            int hr;
            if (MediaControl != null)
            {
                FilterState OldState = GetFilterState();

                TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingEventArgs(
                    Device.DevicePath, FilterStateToString(OldState), FilterStateToString(FilterState.Stopped)));

                hr = MediaControl.Stop();
                
                try
                {
                    DsError.ThrowExceptionForHR(hr);
                    TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangedEventArgs(
                        Device.DevicePath, FilterStateToString(OldState), FilterStateToString(FilterState.Stopped)));
                }
                catch (Exception e)
                {
                    TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingFailedEventArgs(
                        Device.DevicePath, FilterStateToString(OldState), FilterStateToString(FilterState.Stopped)));
                    throw e;
                }
            }
        }

        public virtual void Pause()
        {
            if (MediaControl != null)
            {
                FilterState OldState = GetFilterState();

                TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingEventArgs(
                       Device.DevicePath, FilterStateToString(OldState), FilterStateToString(FilterState.Paused)));

                int hr = MediaControl.Pause();
                try
                {
                    DsError.ThrowExceptionForHR(hr);
                    TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangedEventArgs(
                        Device.DevicePath, FilterStateToString(OldState), FilterStateToString(FilterState.Paused)));
                }
                catch (Exception e)
                {
                    TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingFailedEventArgs(
                        Device.DevicePath, FilterStateToString(OldState), FilterStateToString(FilterState.Paused)));
                    throw e;
                }
            }
        }

        public virtual string GetState()
        {
            return TheRecGraph.FilterStateToString(this.GetFilterState());
        }

        protected virtual FilterState GetFilterState()
        {
            if (MediaControl != null)
            {
                FilterState State;
                int hr = MediaControl.GetState(5000, out State);
                DsError.ThrowExceptionForHR(hr);

                return State;
            }

            return FilterState.Stopped;
        }

        static string FilterStateToString(FilterState State)
        {
            switch (State)
            {
                case FilterState.Running: return "running";
                case FilterState.Paused: return "paused";
                case FilterState.Stopped: return "stopped";
                default: return "unknown";
            }
        }

        public double GetDuration()
        {
            if (MediaPosition == null) return 0;
            double pos = 0;
            int hr = MediaPosition.get_CurrentPosition(out pos);
            if (hr >= 0)
                return pos;
            else return 0;
        }

        public static DateTime DurationToDateTime(double millisec)
        {
            //return new DateTime((long)((int)millisec * TimeSpan.TicksPerMillisecond)); // convert milli sec to nano sec
            int _millisec = (int)millisec;
            int h = _millisec / 3600;
            int m = (_millisec - (h * 3600)) / 60;
            int s = _millisec - (h * 3600 + m * 60);
            
            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, h, m, s);
        }

        public void UpdateVideoWindowPosition(IntPtr VideoWindowOwner, int width, int height)
        {
            Debug.WriteLine(string.Format("videowindow handle {0}", VideoWindowOwner.ToString()));
            if (VideoWindow != null && _DeviceType != DeviceTypeEnum.Audio && _DeviceType != DeviceTypeEnum.Unspecified)
            {
                int hr = 0;
                
                if (VideoWindowOwner != this.VideoWindowOwner)
                {
                    // disable windowed preview
                    hr = VideoWindow.put_AutoShow(OABool.False);

                    if (VideoWindowOwner == IntPtr.Zero)
                    {
                        VideoWindow.put_Visible(OABool.False);
                    }
                    else
                    {
                        hr = VideoWindow.put_Owner(VideoWindowOwner);
                        DsError.ThrowExceptionForHR(hr);

                        hr = VideoWindow.put_WindowStyle(WindowStyle.Child | WindowStyle.ClipSiblings);
                        DsError.ThrowExceptionForHR(hr);

                        VideoWindow.put_Visible(OABool.True);
                    }

                    this.VideoWindowOwner = VideoWindowOwner;
                }

                int posX = 0, posY = 0;
                if (ImageAspectRatio > 0)
                {
                    int widthN = Math.Min(width, (int)(height * ImageAspectRatio));
                    int heightN = Math.Min(height, (int)(width / ImageAspectRatio));
                    posX = (width - widthN) / 2;   // Center within control
                    posY = (height - heightN) / 2;
                    width = widthN; height = heightN;
                }

                hr = VideoWindow.SetWindowPosition(posX, posY, width, height);
                DsError.ThrowExceptionForHR(hr);
            }
        }

        protected void SetVideoParms(ICaptureGraphBuilder2 GraphBuilder, IBaseFilter SourceFilter, int FrameRate, int Width, int Height)
        {
            int hr;
            object o;
            AMMediaType media;
            IAMStreamConfig videoStreamConfig;
            IAMVideoControl videoControl = SourceFilter as IAMVideoControl;

            // Find the stream config interface
            hr = GraphBuilder.FindInterface(null, MediaType.Video, SourceFilter, typeof(IAMStreamConfig).GUID, out o);

            videoStreamConfig = o as IAMStreamConfig;
            try
            {
                if (videoStreamConfig == null)
                {
                    throw new Exception("Failed to get IAMStreamConfig");
                }

                hr = videoStreamConfig.GetFormat(out media);
                DsError.ThrowExceptionForHR(hr);

                // copy out the videoinfoheader
                VideoInfoHeader v = new VideoInfoHeader();
                Marshal.PtrToStructure(media.formatPtr, v);

                // if overriding the framerate, set the frame rate
                if (FrameRate > 0)
                {
                    v.AvgTimePerFrame = 10000000 / FrameRate;
                }

                // if overriding the width, set the width
                if (Width > 0)
                {
                    v.BmiHeader.Width = Width;
                }
                else
                {
                    Width = v.BmiHeader.Width;
                }

                // if overriding the Height, set the Height
                if (Height > 0)
                {
                    v.BmiHeader.Height = Height;
                }
                else
                {
                    Height = v.BmiHeader.Height;
                }

                // Copy the media structure back
                Marshal.StructureToPtr(v, media.formatPtr, false);

                // Set the new format
                hr = videoStreamConfig.SetFormat(media);
                DsError.ThrowExceptionForHR(hr);

                // Format successfully set, calculate image aspect ratio
                ImageAspectRatio = Math.Abs((float)Width / (float)Height);

                DsUtils.FreeAMMediaType(media);
                media = null;

                // Fix upsidedown video
                if (videoControl != null)
                {
                    VideoControlFlags pCapsFlags;

                    IPin pPin = DsFindPin.ByCategory(SourceFilter, PinCategory.Capture, 0);
                    hr = videoControl.GetCaps(pPin, out pCapsFlags);
                    DsError.ThrowExceptionForHR(hr);

                    if ((pCapsFlags & VideoControlFlags.FlipVertical) > 0)
                    {
                        hr = videoControl.GetMode(pPin, out pCapsFlags);
                        DsError.ThrowExceptionForHR(hr);

                        hr = videoControl.SetMode(pPin, 0);
                    }
                }
            }
            finally
            {
                Marshal.ReleaseComObject(videoStreamConfig);
            }
        }

        
        /// <summary>
        /// Handle Graph Events.
        /// </summary>
        public void HandleGraphEvent()
        {
            IntPtr p1, p2;
            EventCode code;
            try
            {
                while (MediaEventEx != null && MediaEventEx.GetEvent(out code, out p1, out p2, 0) == 0)
                {
                    MediaEventEx.FreeEventParams(code, p1, p2);

                    switch (code)
                    {
                        case EventCode.DeviceLost:
                            Stop();
                            Dispose();
                            break;
                        case EventCode.StreamBufferWriteFailure:
                            Stop();
                            Dispose();
                            break;
                        case EventCode.GraphChanged:
                            //Stop();
                            //BuildGraph();
                            //Run();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Console.WriteLine(ex.StackTrace);
#endif
            }
        }

        protected static IBaseFilter GetFilterFromPath(Guid Category, string Path, string Name)
        {
            object result = null;
            Guid iid = typeof(IBaseFilter).GUID;
            foreach (DsDevice Filter in DsDevice.GetDevicesOfCat(Category))
            {
                if (Filter.DevicePath.CompareTo(Path) == 0 ||Filter.Name.CompareTo(Name) == 0)
                {
                    Filter.Mon.BindToObject(null, null, ref iid, out result);
                    break;
                }
            }

            return (IBaseFilter)result;
        }

        public abstract void BuildGraph();
    }

    public class TheRecPreviewGraph : TheRecGraph {

        public TheRecPreviewGraph(DsDevice Device) : base(Device)
        {

        }

        public override void BuildGraph()
        {
            _DeviceType = DeviceTypeEnum.Unspecified;

            int hr;

            IBaseFilter VRenderer = new VideoRendererDefault() as IBaseFilter;
            hr = Graph.AddFilter(VRenderer, null);
            if (hr < 0) throw new Exception();

            hr = GraphBuilder.RenderStream(null, MediaType.Video, SourceFilter, null, VRenderer);

            if (hr >= 0)
            {
                VideoWindow = VRenderer as IVideoWindow;
                hr = VideoWindow.put_AutoShow(OABool.False);
                DsError.ThrowExceptionForHR(hr);

                try
                {
                    SetVideoParms(GraphBuilder, SourceFilter, DEFAULT_FRAMERATE, -1, -1);
                }
                catch (COMException)
                {
                    Debug.WriteLine("Can't set videoparams on " + Device.Name);
                }
            }
            else
            {
                // only video preview!
                DsError.ThrowExceptionForHR(hr);
            }

            _DeviceType = DeviceTypeEnum.Video;
        }
    }

    public class TheRecMPEGCaptureGraph : TheRecGraph
    {
        string OutputPath;
        string OutputFileName;

        public TheRecMPEGCaptureGraph(DsDevice Device, string OutputPath, string OutputFileName) :
            base(Device)
        {
            this.OutputPath = OutputPath;
            this.OutputFileName = OutputFileName;
        }

        public override void BuildGraph()
        {
            int hr;

            // check output path
            if (string.IsNullOrEmpty(OutputPath) || string.IsNullOrEmpty(OutputFileName))
                throw new Exception("Filepath/-name not set!");

            if (!Directory.Exists(OutputPath))
            {
                Directory.CreateDirectory(OutputPath);
            }

            if (File.Exists(string.Format("{0}\\{1}.mpg", OutputPath, OutputFileName)))
            {
                string NewFileName = null; ;
                for (int i = 1; i < 100; i++)
                {
                    if (!File.Exists(string.Format("{0}\\{1}_{2}.mpg", OutputPath, OutputFileName, i)))
                    {
                        NewFileName = string.Format("{0}_{1}", OutputFileName, i);
                        break;
                    }
                }
                if (string.IsNullOrEmpty(NewFileName))
                {
                    throw new Exception(string.Format("Please choose another outputdirectory.\n'{0}.mpg' is allready exists!", OutputFileName));
                }
                else
                {
                    TheRecEventSystem.SendTheRecEvent(this,
                        new TheRecOutputFileNameChangedEventArgs(Device.Name, OutputFileName + ".mpg", NewFileName + ".mpg"));
                    OutputFileName = NewFileName;
                }
            }

            IFileSinkFilter FileWriter = (IFileSinkFilter)new FileWriter();
            Debug.WriteLine(string.Format("output file path={0}", string.Format("{0}\\{1}.mpg", OutputPath, OutputFileName)));
            hr = FileWriter.SetFileName(string.Format("{0}\\{1}.mpg", OutputPath, OutputFileName), null);
            DsError.ThrowExceptionForHR(hr);

            hr = Graph.AddFilter(FileWriter as IBaseFilter, null);
            DsError.ThrowExceptionForHR(hr);

            IBaseFilter Encoder = GetFilterFromPath(FilterCategory.MediaMultiplexerCategory, null, @"Microsoft MPEG-2 Encoder");

            hr = Graph.AddFilter(Encoder, "MPEG Encoder");
            DsError.ThrowExceptionForHR(hr);

            hr = GraphBuilder.RenderStream(null, MediaType.Stream, Encoder, null, FileWriter as IBaseFilter);
            DsError.ThrowExceptionForHR(hr);
            
            ////////////////
            //IFileSinkFilter FileSink;
            //IBaseFilter Mux;
            //hr = GraphBuilder.SetOutputFileName(MediaSubType.Avi, string.Format("{0}\\{1}.mpg", OutputPath, OutputFileName), out Mux, out FileSink);
            //DsError.ThrowExceptionForHR(hr);
            
            //IBaseFilter Encoder = GetFilterFromPath(FilterCategory.VideoCompressorCategory, @"@device:sw:{33D9A760-90C8-11D0-BD43-00A0C911CE86}\ffdshow video encoder", null);
            //hr = Graph.AddFilter(Encoder, "ffdshow encoder");
            //DsError.ThrowExceptionForHR(hr);

            //hr = GraphBuilder.RenderStream(null, null, Encoder, null, Mux);
            //DsError.ThrowExceptionForHR(hr);
            ///////////////

            try
            {
                LinkDVGraph(SourceFilter, Encoder);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                Trace.WriteLine(string.Format("'{0}' is not a DV device!", Device.Name));
            }

            if (DeviceType != DeviceTypeEnum.DV)
            {
                try
                {
                    LinkVideoGraph(SourceFilter, Encoder);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.StackTrace);
                    Trace.WriteLine(string.Format("'{0}' is not a video device!", Device.Name));
                }

                try
                {
                    LinkAudioGraph(SourceFilter, Encoder);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.StackTrace);
                    Trace.WriteLine(string.Format("'{0}' is not a audio device!", Device.Name));
                }
            }
        }

        protected void LinkDVGraph(IBaseFilter SourceFilter, IBaseFilter Encoder)
        {
            int hr;
            IBaseFilter DVSplitter = new DVSplitter() as IBaseFilter;
            hr = Graph.AddFilter(DVSplitter, "DV Splitter");
            if (hr < 0) throw new Exception();

            hr = GraphBuilder.RenderStream(PinCategory.Capture, MediaType.Interleaved, SourceFilter, DVSplitter, Encoder);
            if (hr < 0)
            {
                Graph.RemoveFilter(DVSplitter);
                DsError.ThrowExceptionForHR(hr);
            }

            hr = GraphBuilder.RenderStream(null, null, DVSplitter, null, Encoder);
            if (hr < 0)
            {
                Graph.RemoveFilter(DVSplitter);
                DsError.ThrowExceptionForHR(hr);
            }

            IBaseFilter VideoRenderer = new VideoRenderer() as IBaseFilter;
            hr = Graph.AddFilter(VideoRenderer, "Video Renderer");

            hr = GraphBuilder.RenderStream(PinCategory.Preview, MediaType.Interleaved, SourceFilter, null, VideoRenderer);
            if (hr < 0)
            {
                Graph.RemoveFilter(DVSplitter);
                Graph.RemoveFilter(VideoRenderer);
                DsError.ThrowExceptionForHR(hr);
            }
            else
            {
                VideoWindow = VideoRenderer as IVideoWindow;
                hr = VideoWindow.put_AutoShow(OABool.False);
                DsError.ThrowExceptionForHR(hr);

                try
                {
                    SetVideoParms(GraphBuilder, SourceFilter, -1, -1, -1);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.StackTrace);
                }
            }

            _DeviceType = DeviceTypeEnum.DV;
            Marshal.ReleaseComObject(DVSplitter);
        }

        protected void LinkVideoGraph(IBaseFilter SourceFilter, IBaseFilter Encoder)
        {
            int hr;

            hr = GraphBuilder.RenderStream(PinCategory.Capture, MediaType.Video, SourceFilter, null, Encoder);
            if (hr < 0) DsError.ThrowExceptionForHR(hr);

            IBaseFilter VideoRenderer = new VideoRenderer() as IBaseFilter;
            hr = Graph.AddFilter(VideoRenderer, "Video Renderer");
            if (hr < 0) DsError.ThrowExceptionForHR(hr);

            hr = GraphBuilder.RenderStream(PinCategory.Preview, MediaType.Video, SourceFilter, null, VideoRenderer);
            if (hr < 0)
            {
                Graph.RemoveFilter(VideoRenderer);
                throw new Exception();
            }
            else
            {
                VideoWindow = VideoRenderer as IVideoWindow;
                hr = VideoWindow.put_AutoShow(OABool.False);
                DsError.ThrowExceptionForHR(hr);

                try
                {
                    SetVideoParms(GraphBuilder, SourceFilter, -1, -1, -1);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.StackTrace);
                }
            }

            switch (DeviceType)
            {
                case DeviceTypeEnum.Audio:
                    _DeviceType = DeviceTypeEnum.AV;
                    break;
                case DeviceTypeEnum.Unspecified:
                    _DeviceType = DeviceTypeEnum.Video;
                    break;
            }
        }

        protected void LinkAudioGraph(IBaseFilter SourceFilter, IBaseFilter Encoder)
        {
            int hr;
            hr = GraphBuilder.RenderStream(null, MediaType.Audio, SourceFilter, null, Encoder);
            if (hr < 0) DsError.ThrowExceptionForHR(hr);

            switch (DeviceType)
            {
                case DeviceTypeEnum.Video:
                    _DeviceType = DeviceTypeEnum.AV;
                    break;
                case DeviceTypeEnum.Unspecified:
                    _DeviceType = DeviceTypeEnum.Audio;
                    break;
            }
        }
    }
}
