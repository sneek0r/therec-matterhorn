﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TheRec_Controller;

namespace TheRec_Minimal
{
    /// <summary>
    /// Interaktionslogik für DevicePreviewFrame.xaml
    /// </summary>
    public partial class DevicePreviewFrame : UserControl
    {
        String DeviceName;
        ITheRecGraph2 Graph;

        public DevicePreviewFrame(String DeviceName, ITheRecGraph2 Graph)
        {
            this.DeviceName = DeviceName;
            this.Graph = Graph;

            InitializeComponent();
        }

        private void VideoPreviewPanel_Resize(object sender, EventArgs e)
        {
            UpdateGraphSettings();
        }

        public void UpdateGraphSettings()
        {
            IntPtr Handle = VideoPreviewPanel.Handle;
            int width = VideoPreviewPanel.Width;
            int height = VideoPreviewPanel.Height;

            Graph.UpdateVideoWindowPosition(DeviceName, Handle, width, height);
        }
    }
}
