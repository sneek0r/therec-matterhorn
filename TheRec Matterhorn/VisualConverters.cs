﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;

namespace TheRec_Matterhorn
{
    class AspectRatioConverter : IValueConverter
    {
        public double AspectRatio = 4.0 / 3.0;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter == null || !(parameter is double))
            {
                return (double) value / AspectRatio;
            }
            else
            {
                return (double) value / (double) parameter;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter == null || !(parameter is double))
            {
                return (double)value * AspectRatio;
            }
            else
            {
                return (double)value * (double)parameter;
            }
        }
    }

    public class StateToStartStopText : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.Equals("idle"))
            {
                return "Start Capture";
            }
            else if (value.Equals("capturing"))
            {
                return "Stop Capture";
            }
            else
            {
                return "Please wait!";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class StateIconConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.Equals("idle"))
            {
                return "/TheRec_Matterhorn;component/img/rec30.png";
            }
            else if (value.Equals("capturing"))
            {
                return "/TheRec_Matterhorn;component/img/stop30.png";
            }
            else
            {
                return "/TheRec_Matterhorn;component/img/blank30.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class EmptyStringReplaceConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrEmpty(value as string) && parameter != null)
            {
                return parameter as string;
            }
            else
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && parameter != null && value.Equals(parameter))
                return "";
            else
                return value;
        }
    }
}
