﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using DirectShowLib;

namespace TheRec_Matterhorn_Utils
{
    public static class MHUtils
    {
        public static List<MHDevice> GetAllDevices()
        {
            List<MHDevice> Devices = GetDevicesOfCat(FilterCategory.AudioInputDevice);
            Devices.AddRange(GetVideoDevices());
            return Devices;
        }

        public static List<MHDevice> GetAudioDevices()
        {
            return GetDevicesOfCat(FilterCategory.AudioInputDevice);
        }

        public static List<MHDevice> GetVideoDevices()
        {
            return GetDevicesOfCat(FilterCategory.VideoInputDevice);
        }

        public static List<MHDevice> GetAudioCompressors()
        {
            return GetDevicesOfCat(FilterCategory.AudioCompressorCategory);
        }

        public static List<MHDevice> GetVideoCompressors()
        {
            return GetDevicesOfCat(FilterCategory.VideoCompressorCategory);
        }

        static List<MHDevice> GetDevicesOfCat(Guid FilterCategory)
        {
            List<MHDevice> devices = new List<MHDevice>();
            foreach (DsDevice device in DsDevice.GetDevicesOfCat(FilterCategory))
            {
                MHDevice MHDevice;
                if (FilterCategory == DirectShowLib.FilterCategory.AudioInputDevice)
                {
                    MHDevice = new MHDevice(device, IsAudio:true);
                }
                else if (FilterCategory == DirectShowLib.FilterCategory.VideoInputDevice)
                {
                    MHDevice = new MHDevice(device, IsVideo: true);
                } 
                else if (FilterCategory == DirectShowLib.FilterCategory.AudioCompressorCategory)
                {
                    MHDevice = new MHDevice(device, IsAudio: true, IsEncoder: true);
                }
                else if (FilterCategory == DirectShowLib.FilterCategory.VideoCompressorCategory)
                {
                    MHDevice = new MHDevice(device, IsVideo: true, IsEncoder: true);
                } 
                else
                {
                    MHDevice = new MHDevice(device);
                }

                devices.Add(MHDevice);
            }
            return devices;
        }

        public static DsDevice GetDsDevice(string Path)
        {
            List<DsDevice> Devices = DsDevice.GetDevicesOfCat(FilterCategory.AudioInputDevice).ToList();
            Devices.AddRange(DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice).ToList());
            foreach (DsDevice Device in Devices.Where(dev => dev.DevicePath == Path))
            {
                return Device;
            }
            return null;
        }
    }

    public class MHDebug
    {
        public static void WriteLine(string Format, params string[] args) 
        {
            string Time = DateTime.Now.ToString("hh:mm:ss:fff ");
            Debug.WriteLine(Time + Format, args);
        }
    }
}
