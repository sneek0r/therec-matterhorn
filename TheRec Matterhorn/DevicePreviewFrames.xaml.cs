﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TheRec_Matterhorn_Utils;

namespace TheRec_Matterhorn
{
    /// <summary>
    /// Interaktionslogik für DevicePreviewFrames.xaml
    /// </summary>
    public partial class DevicePreviewFrames : UserControl
    {
        Dictionary<MHDevice, MainDevicePreviewFrame> PreviewFrames = new Dictionary<MHDevice, MainDevicePreviewFrame>();

        public DevicePreviewFrames()
        {
            InitializeComponent();

            MHEventSystem.TheRecMHRecordingPreviewStartEvent += new TheRecMHEventHandler<TheRecMHRecordingPreviewStartEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingPreviewStartedEvent += new TheRecMHEventHandler<TheRecMHRecordingPreviewStartedEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingPreviewStopEvent += new TheRecMHEventHandler<TheRecMHRecordingPreviewStopEventArgs>(this.OnTheRecMHEvent);

            MHEventSystem.TheRecMHRecordingCaptureStartEvent += new TheRecMHEventHandler<TheRecMHRecordingCaptureStartEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingCaptureStartedEvent += new TheRecMHEventHandler<TheRecMHRecordingCaptureStartedEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingCaptureStopEvent += new TheRecMHEventHandler<TheRecMHRecordingCaptureStopEventArgs>(this.OnTheRecMHEvent);
        }


        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            MHEventSystem.TheRecMHRecordingPreviewStartEvent -= new TheRecMHEventHandler<TheRecMHRecordingPreviewStartEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingPreviewStartedEvent -= new TheRecMHEventHandler<TheRecMHRecordingPreviewStartedEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingPreviewStopEvent -= new TheRecMHEventHandler<TheRecMHRecordingPreviewStopEventArgs>(this.OnTheRecMHEvent);

            MHEventSystem.TheRecMHRecordingCaptureStartEvent -= new TheRecMHEventHandler<TheRecMHRecordingCaptureStartEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingCaptureStartedEvent -= new TheRecMHEventHandler<TheRecMHRecordingCaptureStartedEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingCaptureStopEvent -= new TheRecMHEventHandler<TheRecMHRecordingCaptureStopEventArgs>(this.OnTheRecMHEvent);
        }

        void CreatePreviewFrames()
        {
            if (DataContext is List<MHDevice>)
            {
                foreach (MHDevice Device in DataContext as List<MHDevice>)
                {
                    MainDevicePreviewFrame PreviewFrame = new MainDevicePreviewFrame();
                    PreviewFrame.DataContext = Device;

                    PreviewFrames.Add(Device, PreviewFrame);
                }
            }
        }

        void DestroyPreviewFrames()
        {
            PreviewFrames.Clear();
        }

        void ShowPreviewFrames(bool ActiveDevicesOnly = false)
        {
            if (DataContext is List<MHDevice>)
            {
                List<MHDevice> Devices = DataContext as List<MHDevice>;
                if (ActiveDevicesOnly)
                {
                    Devices = Devices.Where(dev => dev.IsVideo && dev.Active).ToList();
                }
                else
                {
                    Devices = Devices.Where(dev => dev.IsVideo).ToList();
                }

                int count = Devices.Count;
                int columns = count > 2 ? 2 : 1;
                int rows = count / columns + count % columns;

                for (int r = 0; r < rows; r++)
                {
                    RowDefinition Row = new RowDefinition();
                    Row.Height = new GridLength(1, GridUnitType.Star);
                    PreviewFramesGrid.
                }

                    for (int w = 0; w < columns; w++)
                    {
                        ColumnDefinition Column = new ColumnDefinition();
                        Column.Width = new GridLength(1, GridUnitType.Star);
                        ColumnDefinitions.Add(Column);

                        for (int h = 0; h < rows; h++)
                        {

                        }
                    }
                    
            }
        }

        void HidePreviewFrames()
        {
            PreviewFramesGrid.Visibility = System.Windows.Visibility.Hidden;
        }


        public void OnTheRecMHEvent(object sender, TheRecMHEventArgs eventArgs)
        {
            if (eventArgs is TheRecMHRecordingPreviewStartEventArgs)
            {
                
            }
            else if (eventArgs is TheRecMHRecordingPreviewStartedEventArgs)
            {
               
            }
            else if (eventArgs is TheRecMHRecordingPreviewStopEventArgs)
            {
                
            }
            else if (eventArgs is TheRecMHRecordingCaptureStartEventArgs)
            {

            }
            else if (eventArgs is TheRecMHRecordingCaptureStartedEventArgs)
            {

            }
            else if (eventArgs is TheRecMHRecordingCaptureStopEventArgs)
            {

            }
        }

    }
}
