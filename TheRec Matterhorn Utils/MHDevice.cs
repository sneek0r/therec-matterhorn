﻿using System;
using System.Linq;
using DirectShowLib;
using TheRec_Controller;

namespace TheRec_Matterhorn_Utils
{
    [Serializable()]
    public class MHDevice : IEquatable<MHDevice>, IDisposable
    {
        public static string[] FlavourTypes { get { return new string[] { "presenter", "presentation", "data", "audience" }; } }

        public bool Active { get { return Enabled && _Active; } set { _Active = value; } }
        [field:NonSerialized()]
        private bool _Active = true;
        public bool Enabled { get; set; }
        public string Name { get { return _Name; } }
        private string _Name;
        public string Path { get { return _Path; } }
        private string _Path;
        public string FriendlyName { get; set; }
        public string Flavour { get { return _Flavour != null ? _Flavour : FlavourTypes.First(); } set { _Flavour = value; } }
        private string _Flavour;

        public bool IsAudio { get { return _IsAudio; } }
        public bool IsVideo { get { return _IsVideo; } }
        public bool IsEncoder { get { return _IsEncoder; } }
        private bool _IsAudio;
        private bool _IsVideo;
        private bool _IsEncoder;

        public string FileName { get { return _FileName; } }
        [field:NonSerialized()]
        protected string _FileName = null;

        [field: NonSerialized()]
        protected ITheRecGraph3 Graph = null;
        public bool Capturing { get { return Graph != null && Graph.IsCaptureGraph() && Graph.GetState().Equals("running"); } }
        
        public MHDevice(DsDevice Device, bool IsAudio = false, bool IsVideo = false, bool IsEncoder = false)
        {
            this._Name = Device.Name;
            this._Path = Device.DevicePath;
            this._IsAudio = IsAudio;
            this._IsVideo = IsVideo;
            this._IsEncoder = IsEncoder;
            this.FriendlyName = Device.Name;

            TheRec_Controller.TheRecEventSystem.GraphStateChangingEvent += new TheRecEventHandler<TheRecGraphStateChangingEventArgs>(this.OnTheRecEvent);
            TheRec_Controller.TheRecEventSystem.GraphStateChangingFailedEvent += new TheRecEventHandler<TheRecGraphStateChangingFailedEventArgs>(this.OnTheRecEvent);
            TheRec_Controller.TheRecEventSystem.GraphStateChangedEvent += new TheRecEventHandler<TheRecGraphStateChangedEventArgs>(this.OnTheRecEvent);
            TheRec_Controller.TheRecEventSystem.GraphErrorEvent += new TheRecEventHandler<TheRecGraphErrorEventArgs>(this.OnTheRecEvent);
        }

        public void Dispose()
        {
            if (Graph != null)
            {
                Graph.Stop();
            }

            TheRec_Controller.TheRecEventSystem.GraphStateChangingEvent -= new TheRecEventHandler<TheRecGraphStateChangingEventArgs>(this.OnTheRecEvent);
            TheRec_Controller.TheRecEventSystem.GraphStateChangingFailedEvent -= new TheRecEventHandler<TheRecGraphStateChangingFailedEventArgs>(this.OnTheRecEvent);
            TheRec_Controller.TheRecEventSystem.GraphStateChangedEvent -= new TheRecEventHandler<TheRecGraphStateChangedEventArgs>(this.OnTheRecEvent);
            TheRec_Controller.TheRecEventSystem.GraphErrorEvent += new TheRecEventHandler<TheRecGraphErrorEventArgs>(this.OnTheRecEvent);
        }

        public bool Equals(MHDevice other)
        {
            return Path.Equals(other.Path);
        }

        public DsDevice GetDsDevice()
        {
            return MHUtils.GetDsDevice(Path);
        }

        public static string FlavourToFileNamePostfix(string Flavour) 
        {
            switch (Flavour)
            {
                case "presenter": return "_VIDEO";
                case "presentation": return "_VGA";
                case "data": return "_DATA";
                case "audience": return "_AUDIENCE";
                default: return "";
            }
        }

        public void BuildCaptureGraph(string OutputPath)
        {
            if (Graph != null)
                StopGraph();

            Graph = new TheRecCaptureMPEGGraph3();
            try
            {
                Graph.AddSource(Path, FriendlyName, FlavourToFileNamePostfix(Flavour), OutputPath);
            }
            catch (Exception e)
            {
                MHDebug.WriteLine(e.Message);
                Graph.Dispose();
                Graph = null;
                throw e;
            }
        }

        public void BuildPreviewGraph()
        {
            if (Graph != null)
                StopGraph();

            Graph = new TheRecPreviewGraph3();
            try
            {
                Graph.AddSource(Path, FriendlyName, null, null);
            }
            catch (Exception e)
            {
                MHDebug.WriteLine(e.Message);
                Graph.Dispose();
                Graph = null;
                throw e;
            }
        }

        public void RunGraph()
        {
            Graph.Run();
        }

        public void StopGraph()
        {
            if (Graph == null) return;
            try
            {
                Graph.Stop();
            }
            catch (Exception e)
            {
                MHDebug.WriteLine(e.Message);
            }
            finally
            {
                Graph.Dispose();
                Graph = null;
            }
        }

        public DateTime GetDuration()
        {
            double Duration = 0d;
            if (Capturing) Duration = Graph.GetDuration();
            return TheRecGraph3.DurationToDateTime(Duration);
        }

        public void UpdateVideoWindow(IntPtr PanleHandle, int width, int height)
        {
            if (Graph == null) return;

            try
            {
                Graph.UpdateVideoWindowPosition(PanleHandle, width, height);
            }
            catch (Exception e)
            {
                MHDebug.WriteLine(e.Message);
            }
        }

        public void OnTheRecEvent(object sender, TheRecEventArgs eventArgs)
        {
            if (eventArgs is TheRecGraphStateChangingEventArgs)
            {
                TheRecGraphStateChangingEventArgs changingArgs = eventArgs as TheRecGraphStateChangingEventArgs;
                if (Path.Equals(changingArgs.DevicePath))
                {
                    MHEventSystem.SendTheRecMHEvent(this, new TheRecMHDeviceStateChangingEventArgs(this, changingArgs.OldState, changingArgs.NewState));
                }
            } 
            else if (eventArgs is TheRecGraphStateChangingFailedEventArgs)
            {
                TheRecGraphStateChangingFailedEventArgs changingFailedArgs = eventArgs as TheRecGraphStateChangingFailedEventArgs;
                if (Path.Equals(changingFailedArgs.DevicePath))
                {
                    MHEventSystem.SendTheRecMHEvent(this, new TheRecMHDeviceStateChangingFailedEventArgs(this, changingFailedArgs.OldState, changingFailedArgs.NewState));
                }
            } 
            else if (eventArgs is TheRecGraphStateChangedEventArgs)
            {
                TheRecGraphStateChangedEventArgs changedArgs = eventArgs as TheRecGraphStateChangedEventArgs;
                if (Path.Equals(changedArgs.DevicePath))
                {
                    MHEventSystem.SendTheRecMHEvent(this, new TheRecMHDeviceStateChangedEventArgs(this, changedArgs.OldState, changedArgs.NewState));
                }
            }
            else if (eventArgs is TheRecGraphErrorEventArgs)
            {
                StopGraph();
            }
        }
    }
}
