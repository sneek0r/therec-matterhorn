﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using TheRec_Matterhorn_Utils;

namespace TheRec_Matterhorn
{
    /// <summary>
    /// Interaktionslogik für MainContentFrame.xaml
    /// </summary>
    public partial class MainContentFrame : UserControl
    {
        public MainContentFrame()
        {
            InitializeComponent();

            //MHEventSystem.TheRecMHGraphStateChangingEvent += new TheRecMHEventHandler<TheRecMHGraphStateChangingEventArgs>(this.OnTheRecMHEvent);
            //MHEventSystem.TheRecMHGraphStateChangedEvent += new TheRecMHEventHandler<TheRecMHGraphStateChangedEventArgs>(this.OnTheRecMHEvent);

            MHEventSystem.TheRecMHRecordingCaptureStartEvent += new TheRecMHEventHandler<TheRecMHRecordingCaptureStartEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingCaptureStopEvent += new TheRecMHEventHandler<TheRecMHRecordingCaptureStopEventArgs>(this.OnTheRecMHEvent);

            MHEventSystem.TheRecMHDeviceCaptureStartedEvent += new TheRecMHEventHandler<TheRecMHDeviceCaptureStartedEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHDevicePreviewStartedEvent += new TheRecMHEventHandler<TheRecMHDevicePreviewStartedEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingCaptureStopEvent += new TheRecMHEventHandler<TheRecMHRecordingCaptureStopEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingPreviewStopEvent += new TheRecMHEventHandler<TheRecMHRecordingPreviewStopEventArgs>(this.OnTheRecMHEvent);
        }
        
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            //MHEventSystem.TheRecMHGraphStateChangingEvent -= new TheRecMHEventHandler<TheRecMHGraphStateChangingEventArgs>(this.OnTheRecMHEvent);
            //MHEventSystem.TheRecMHGraphStateChangedEvent -= new TheRecMHEventHandler<TheRecMHGraphStateChangedEventArgs>(this.OnTheRecMHEvent);

            MHEventSystem.TheRecMHRecordingCaptureStartEvent -= new TheRecMHEventHandler<TheRecMHRecordingCaptureStartEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingCaptureStopEvent -= new TheRecMHEventHandler<TheRecMHRecordingCaptureStopEventArgs>(this.OnTheRecMHEvent);

            MHEventSystem.TheRecMHDeviceCaptureStartedEvent -= new TheRecMHEventHandler<TheRecMHDeviceCaptureStartedEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHDevicePreviewStartedEvent -= new TheRecMHEventHandler<TheRecMHDevicePreviewStartedEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingCaptureStopEvent -= new TheRecMHEventHandler<TheRecMHRecordingCaptureStopEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingPreviewStopEvent -= new TheRecMHEventHandler<TheRecMHRecordingPreviewStopEventArgs>(this.OnTheRecMHEvent);
        }

        public void OnTheRecMHEvent(object sender, TheRecMHEventArgs eventArgs)
        {
            if (eventArgs is TheRecMHDeviceCaptureStartedEventArgs)
            {
                TheRecMHDeviceCaptureStartedEventArgs startedEvArgs = eventArgs as TheRecMHDeviceCaptureStartedEventArgs;
                if (startedEvArgs.Device.IsVideo)
                {
                    AddPreviewForDevice(startedEvArgs.Device);
                }
            }
            if (eventArgs is TheRecMHDevicePreviewStartedEventArgs)
            {
                TheRecMHDevicePreviewStartedEventArgs startedEvArgs = eventArgs as TheRecMHDevicePreviewStartedEventArgs;
                if (startedEvArgs.Device.IsVideo)
                {
                    AddPreviewForDevice(startedEvArgs.Device);
                }
            }
            else if (eventArgs is TheRecMHRecordingCaptureStopEventArgs
                || eventArgs is TheRecMHRecordingPreviewStopEventArgs
                || eventArgs is TheRecMHRecordingCaptureStartEventArgs
                || eventArgs is TheRecMHRecordingCaptureStopEventArgs)
            {
                ClearPreviewGrid();
            }

            //if (!(DataContext is MHRecording)) return;

            //if (eventArgs is TheRecMHGraphStateChangingEventArgs)
            //{
            //    TheRecMHGraphStateChangingEventArgs graphChangingEvArgs = eventArgs as TheRecMHGraphStateChangingEventArgs;
            //    if ("stopped".Equals(graphChangingEvArgs.NewState))
            //    {
            //        // close video preview
            //        ClearPreviewGrid(DataContext as MHRecording);
            //    }
            //}
            //else if (eventArgs is TheRecMHGraphStateChangedEventArgs)
            //{
            //    TheRecMHGraphStateChangedEventArgs graphChangedEvArgs = eventArgs as TheRecMHGraphStateChangedEventArgs;
            //    if ("running".Equals(graphChangedEvArgs.NewState))
            //    {
            //        // start video preview
            //        FillPreviewGrid(DataContext as MHRecording);
            //    }
            //}
        }

        private void StartStopButton_Click(object sender, RoutedEventArgs e)
        {
            MHDebug.WriteLine("Main ContentFrame StartStopButtonClick");

            MHRecording Recording = (MHRecording)DataContext;
            if (Recording == null) return;

            switch (Recording.State)
            {
                case MHRecording.STATE_IDLE:
                    if (Recording.OptionsModel.Devices.Where(dev => dev.Active).ToList().Count > 0)
                        MainWindow.Instance.StartCapture();
                    else
                    {
                        MessageBox.Show("Please select a capture device!", "Select capture device", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    }
                    break;
                case MHRecording.STATE_CAPTURING:
                    MainWindow.Instance.StopCapture();
                    break;
                case MHRecording.STATE_STARTING:
                    if (MessageBox.Show("I am start capturing. Do you want to cancel it?", "Stop capture start?",
                        MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    {
                        MainWindow.Instance.StopCapture();
                    }
                    break;
                case MHRecording.STATE_STOPPING:
                    break;
            }
        }

        private void Grid_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is MHRecording && SeriesComboBox != null)
            {
                FillSeriesCombobox(e.NewValue as MHRecording);
                //StartPreview(e.NewValue as MHRecording);
            }
        }

        private void FillSeriesCombobox(MHRecording Recording)
        {
            if (SeriesComboBox != null)
            {
                SeriesComboBox.Items.Clear();
                SeriesComboBox.Items.Insert(0, "<select one>");
                SeriesComboBox.SelectedIndex = 0;

                if (Directory.Exists(Recording.OptionsModel.CaptureOutputPath))
                {
                    foreach (string Serie in Directory.GetDirectories(Recording.OptionsModel.CaptureOutputPath))
                    {
                        string Entry = new DirectoryInfo(Serie).Name;
                        if (!MHRecording.DEFAULT_SERIES_NAME.Equals(Entry))
                            SeriesComboBox.Items.Add(Entry);
                    }
                }
            }
        }

        void AddPreviewForDevice(MHDevice Device) 
        {
            if (Device == null) return;
            MainDevicePreviewFrame PreviewFrame = new MainDevicePreviewFrame(Device);
            PreviewGrid.Dispatcher.Invoke(new Action(delegate()
            {
                PreviewGrid.Children.Add(PreviewFrame);
                PreviewGrid.Columns = PreviewGrid.Children.Count > 2 ? 2 : 1;
                PreviewGrid.Rows = PreviewGrid.Columns == 1 ? PreviewGrid.Children.Count :
                    PreviewGrid.Children.Count / PreviewGrid.Columns + PreviewGrid.Children.Count % PreviewGrid.Columns;
            }));
        }

        //protected void FillPreviewGrid(MHRecording Recording)
        //{
        //    if (Recording == null) return;
        //    ClearPreviewGrid(Recording);

        //    List<MHDevice> VideoDevices;
        //    if (Recording.IsCapturing)
        //    {
        //        VideoDevices = Recording.OptionsModel.Devices.Where(dev => dev.Active && dev.Enabled && dev.IsVideo).ToList<MHDevice>();
        //    }
        //    else
        //    {
        //        VideoDevices = Recording.OptionsModel.Devices.Where(dev => dev.Enabled && dev.IsVideo).ToList<MHDevice>();
        //    }

        //    foreach (MHDevice Device in VideoDevices)
        //    {
        //        MainDevicePreviewFrame PreviewFrame = new MainDevicePreviewFrame(Device.FriendlyName, Recording);
        //        PreviewGrid.Dispatcher.Invoke(new Action(delegate()
        //        {
        //            PreviewGrid.Children.Add(PreviewFrame);
        //            PreviewGrid.Columns = PreviewGrid.Children.Count > 2 ? 2 : 1;
        //            PreviewGrid.Rows = PreviewGrid.Columns == 1 ? PreviewGrid.Children.Count : 
        //                PreviewGrid.Children.Count / PreviewGrid.Columns + PreviewGrid.Children.Count % PreviewGrid.Columns;
        //        }));
        //    }
        //}

        protected void ClearPreviewGrid()
        {
            MHDebug.WriteLine("Main ContentFrame StopPreview");
            PreviewGrid.Dispatcher.Invoke(new Action(delegate()
            {
                PreviewGrid.Children.Clear();
                PreviewGrid.Columns = 1;
                PreviewGrid.Rows = 1;
            }));
        }
    }
}
