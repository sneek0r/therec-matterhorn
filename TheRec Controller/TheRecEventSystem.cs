﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheRec_Controller
{
    public delegate void TheRecEventHandler<TEventArgs>(Object sender, TEventArgs e) where TEventArgs : TheRecEventArgs;

    public interface ITheRecEventListener
    {
        void OnTheRecEvent(object sender, TheRecEventArgs eventArgs);
    }

    public class TheRecEventSystem
    {
        public static event TheRecEventHandler<TheRecEventArgs> TheRecEvent;
        public static void SendTheRecEvent(object sender, TheRecEventArgs eventArgs)
        {
            TheRecEventHandler<TheRecEventArgs> tmpEvt = TheRecEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }
        
        public static event TheRecEventHandler<TheRecGraphStateChangingEventArgs> GraphStateChangingEvent;
        public static void SendTheRecEvent(object sender, TheRecGraphStateChangingEventArgs eventArgs)
        {
            TheRecEventHandler<TheRecGraphStateChangingEventArgs> tmpEvt = GraphStateChangingEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecEventHandler<TheRecGraphStateChangedEventArgs> GraphStateChangedEvent;
        public static void SendTheRecEvent(object sender, TheRecGraphStateChangedEventArgs eventArgs)
        {
            TheRecEventHandler<TheRecGraphStateChangedEventArgs> tmpEvt = GraphStateChangedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecEventHandler<TheRecGraphStateChangingFailedEventArgs> GraphStateChangingFailedEvent;
        public static void SendTheRecEvent(object sender, TheRecGraphStateChangingFailedEventArgs eventArgs)
        {
            TheRecEventHandler<TheRecGraphStateChangingFailedEventArgs> tmpEvt = GraphStateChangingFailedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecEventHandler<TheRecOutputFileNameChangedEventArgs> OutputFileNameChangedEvent;
        public static void SendTheRecEvent(object sender, TheRecOutputFileNameChangedEventArgs eventArgs)
        {
            TheRecEventHandler<TheRecOutputFileNameChangedEventArgs> tmpEvt = OutputFileNameChangedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecEventHandler<TheRecGraphErrorEventArgs> GraphErrorEvent;
        public static void SendTheRecEvent(object sender, TheRecGraphErrorEventArgs eventArgs)
        {
            TheRecEventHandler<TheRecGraphErrorEventArgs> tmpEvt = GraphErrorEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }
    }

    public class TheRecEventArgs : EventArgs
    {
        public readonly string Message;

        public TheRecEventArgs(string Message)
        {
            this.Message = Message;
        }

        public override string ToString()
        {
            return Message;
        }
    }

    public class TheRecGraphStateChangingEventArgs : TheRecEventArgs
    {
        public readonly string DevicePath;
        public readonly string OldState;
        public readonly string NewState;

        public TheRecGraphStateChangingEventArgs(string DevicePath, string OldState, string NewState) :
            base(string.Format("Graph state changing from '{0}' to '{1}'.", OldState, NewState))
        {
            this.DevicePath = DevicePath;
            this.OldState = OldState;
            this.NewState = NewState;
        }
    }

    public class TheRecGraphStateChangingFailedEventArgs : TheRecEventArgs
    {
        public readonly string DevicePath;
        public readonly string OldState;
        public readonly string NewState;

        public TheRecGraphStateChangingFailedEventArgs(string DevicePath, string OldState, string NewState) :
            base(string.Format("Graph state changing from '{0}' to '{1}' failed!", OldState, NewState))
        {
            this.DevicePath = DevicePath;
            this.OldState = OldState;
            this.NewState = NewState;
        }
    }

    public class TheRecGraphStateChangedEventArgs : TheRecEventArgs
    {
        public readonly string DevicePath;
        public readonly string OldState;
        public readonly string NewState;

        public TheRecGraphStateChangedEventArgs(string DevicePath, string OldState, string NewState) : 
            base(string.Format("Graph state has changed from '{0}' to '{1}'.", OldState, NewState))
        {
            this.DevicePath = DevicePath;
            this.OldState = OldState;
            this.NewState = NewState;
        }
    }
    
    public class TheRecOutputFileNameChangedEventArgs : TheRecEventArgs
    {
        public readonly string DeviceName;
        public readonly string OldFileName;
        public readonly string NewFileName;

        public TheRecOutputFileNameChangedEventArgs(string DeviceName, string OldFileName, string NewFileName) :
            base(string.Format("Output filename for '{2}' changed from '{0}' to '{1}'!", OldFileName, NewFileName, DeviceName))
        {
            this.DeviceName = DeviceName;
            this.OldFileName = OldFileName;
            this.NewFileName = NewFileName;
        }
    }

    public class TheRecGraphErrorEventArgs : TheRecEventArgs
    {
        public readonly string DevicePath;

        public TheRecGraphErrorEventArgs(string DevicePath)
            : base(string.Format("Error occurred on '{0}'", DevicePath))
        {
            this.DevicePath = DevicePath;
        }
    }
}
