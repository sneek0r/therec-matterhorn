﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using TheRec_Matterhorn_Utils;

namespace TheRec_Matterhorn
{
    /// <summary>
    /// Interaktionslogik für PreviewGrid.xaml
    /// </summary>
    public partial class PreviewGrid : UniformGrid
    {
        Dictionary<MHDevice, MainDevicePreviewFrame> PreviewFrames = new Dictionary<MHDevice, MainDevicePreviewFrame>();

        public PreviewGrid()
        {
            InitializeComponent();

        }

        void CreatePreviewFrames()
        {
            if (DataContext is List<MHDevice>)
            {
                foreach (MHDevice Device in DataContext as List<MHDevice>)
                {
                    if (Device.Enabled && Device.IsVideo)
                    {
                        MainDevicePreviewFrame PreviewFrame = new MainDevicePreviewFrame();
                        PreviewFrame.DataContext = Device;

                        PreviewFrames.Add(Device, PreviewFrame);
                    }
                }
            }
        }

        void DestroyPreviewFrames()
        {
            MainDevicePreviewFrame Frame = PreviewFrames.First<MainDevicePreviewFrame>;
            while()
        }

        void ShowPreviewFrames(bool ActiveDevicesOnly = false)
        {
            if (DataContext is List<MHDevice>)
            {
                List<MHDevice> Devices = DataContext as List<MHDevice>;
                if (ActiveDevicesOnly)
                {
                    Devices = Devices.Where(dev => dev.IsVideo && dev.Active).ToList();
                }
                else
                {
                    Devices = Devices.Where(dev => dev.IsVideo).ToList();
                }

                int count = Devices.Count;
                Columns = count > 2 ? 2 : 1;
                Rows = count / Columns + count % Columns;

                foreach (MHDevice Device in Devices)
                {
                    MainDevicePreviewFrame frame = PreviewFrames[Device];
                    Children.Add(frame);
                    frame.Visibility = System.Windows.Visibility.Visible;
                }
            }
        }

        void HidePreviewFrames()
        {
            PreviewFramesGrid.Visibility = System.Windows.Visibility.Hidden;
        }


        public void OnTheRecMHEvent(object sender, TheRecMHEventArgs eventArgs)
        {
            if (eventArgs is TheRecMHRecordingPreviewStartEventArgs)
            {

            }
            else if (eventArgs is TheRecMHRecordingPreviewStartedEventArgs)
            {

            }
            else if (eventArgs is TheRecMHRecordingPreviewStopEventArgs)
            {

            }
            else if (eventArgs is TheRecMHRecordingCaptureStartEventArgs)
            {

            }
            else if (eventArgs is TheRecMHRecordingCaptureStartedEventArgs)
            {

            }
            else if (eventArgs is TheRecMHRecordingCaptureStopEventArgs)
            {

            }
        }
    }
}
