﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TheRec_Matterhorn_Utils;

namespace TheRec_Matterhorn
{
    /// <summary>
    /// Interaktionslogik für MainDevicePreviewFrame.xaml
    /// </summary>
    public partial class MainDevicePreviewFrame : UserControl
    {
        MHDevice Device;

        public MainDevicePreviewFrame(MHDevice Device)
        {
            this.Device = Device;
            InitializeComponent();

            PreviewGroupBox.Header = Device.FriendlyName;
            UpdateFrameSize();
        }

        public void UpdateFrameSize(bool Closing = false)
        {
            if (Device != null)
            {
                IntPtr Handle = IntPtr.Zero;
                int height = -1;
                int width = -1;

                if (!Closing)
                {
                    Handle = VideoPreviewPanel.Handle;
                    width = VideoPreviewPanel.Width;
                    height = VideoPreviewPanel.Height;
                }

                try
                {
                    Device.UpdateVideoWindow(Handle, width, height);
                }
                catch (Exception e)
                {
                    MHDebug.WriteLine("ERROR while updating preview frame: " + e.StackTrace);
                }
            }
        }

        private void Grid_Unloaded(object sender, RoutedEventArgs e)
        {
            MHDebug.WriteLine("previewframe unload");
            //UpdateFrameSize(Closing: true);
        }

        private void VideoPreviewPanel_Resize(object sender, EventArgs e)
        {
            UpdateFrameSize();
        }
    }
}
