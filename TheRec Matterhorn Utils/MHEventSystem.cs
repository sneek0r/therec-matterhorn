﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheRec_Matterhorn_Utils
{
    public delegate void TheRecMHEventHandler<TEventArgs>(Object sender, TEventArgs e) where TEventArgs : TheRecMHEventArgs;

    public interface ITheRecMHEventListener
    {
        void OnTheRecMHEvent(object sender, TheRecMHEventArgs eventArgs);
    }

    public abstract class TheRecMHEventArgs : EventArgs
    {
        public readonly string Message;

        public TheRecMHEventArgs(string Message) : base()
        { 
            this.Message = Message;
        }

        public override string ToString()
        {
            return Message;
        }
    }

    public class MHEventSystem
    {
        public static event TheRecMHEventHandler<TheRecMHGraphStateChangingEventArgs> TheRecMHGraphStateChangingEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHGraphStateChangingEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHGraphStateChangingEventArgs> tmpEvt = TheRecMHGraphStateChangingEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHGraphStateChangingFailedEventArgs> TheRecMHGraphStateChangingFailedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHGraphStateChangingFailedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHGraphStateChangingFailedEventArgs> tmpEvt = TheRecMHGraphStateChangingFailedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHGraphStateChangedEventArgs> TheRecMHGraphStateChangedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHGraphStateChangedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHGraphStateChangedEventArgs> tmpEvt = TheRecMHGraphStateChangedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHRecordingCaptureStartEventArgs> TheRecMHRecordingCaptureStartEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHRecordingCaptureStartEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHRecordingCaptureStartEventArgs> tmpEvt = TheRecMHRecordingCaptureStartEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHRecordingCaptureStopEventArgs> TheRecMHRecordingCaptureStopEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHRecordingCaptureStopEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHRecordingCaptureStopEventArgs> tmpEvt = TheRecMHRecordingCaptureStopEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHRecordingPreviewStartEventArgs> TheRecMHRecordingPreviewStartEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHRecordingPreviewStartEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHRecordingPreviewStartEventArgs> tmpEvt = TheRecMHRecordingPreviewStartEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHRecordingPreviewStopEventArgs> TheRecMHRecordingPreviewStopEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHRecordingPreviewStopEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHRecordingPreviewStopEventArgs> tmpEvt = TheRecMHRecordingPreviewStopEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHRecordingCaptureStartedEventArgs> TheRecMHRecordingCaptureStartedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHRecordingCaptureStartedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHRecordingCaptureStartedEventArgs> tmpEvt = TheRecMHRecordingCaptureStartedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHRecordingCaptureStoppedEventArgs> TheRecMHRecordingCaptureStoppedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHRecordingCaptureStoppedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHRecordingCaptureStoppedEventArgs> tmpEvt = TheRecMHRecordingCaptureStoppedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHRecordingPreviewStartedEventArgs> TheRecMHRecordingPreviewStartedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHRecordingPreviewStartedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHRecordingPreviewStartedEventArgs> tmpEvt = TheRecMHRecordingPreviewStartedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHRecordingPreviewStoppedEventArgs> TheRecMHRecordingPreviewStoppedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHRecordingPreviewStoppedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHRecordingPreviewStoppedEventArgs> tmpEvt = TheRecMHRecordingPreviewStoppedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHRecordingCaptureStartFailedEventArgs> TheRecMHRecordingCaptureStartFailedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHRecordingCaptureStartFailedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHRecordingCaptureStartFailedEventArgs> tmpEvt = TheRecMHRecordingCaptureStartFailedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHRecordingPreviewStartFailedEventArgs> TheRecMHRecordingPreviewStartFailedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHRecordingPreviewStartFailedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHRecordingPreviewStartFailedEventArgs> tmpEvt = TheRecMHRecordingPreviewStartFailedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHDeviceStateChangingEventArgs> TheRecMHDeviceStateChangingEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHDeviceStateChangingEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHDeviceStateChangingEventArgs> tmpEvt = TheRecMHDeviceStateChangingEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHDeviceStateChangedEventArgs> TheRecMHDeviceStateChangedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHDeviceStateChangedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHDeviceStateChangedEventArgs> tmpEvt = TheRecMHDeviceStateChangedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHDeviceStateChangingFailedEventArgs> TheRecMHDeviceStateChangingFailedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHDeviceStateChangingFailedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHDeviceStateChangingFailedEventArgs> tmpEvt = TheRecMHDeviceStateChangingFailedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHDeviceCaptureStartedEventArgs> TheRecMHDeviceCaptureStartedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHDeviceCaptureStartedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHDeviceCaptureStartedEventArgs> tmpEvt = TheRecMHDeviceCaptureStartedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHDeviceCaptureStoppedEventArgs> TheRecMHDeviceCaptureStoppedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHDeviceCaptureStoppedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHDeviceCaptureStoppedEventArgs> tmpEvt = TheRecMHDeviceCaptureStoppedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHDevicePreviewStartedEventArgs> TheRecMHDevicePreviewStartedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHDevicePreviewStartedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHDevicePreviewStartedEventArgs> tmpEvt = TheRecMHDevicePreviewStartedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHDevicePreviewStoppedEventArgs> TheRecMHDevicePreviewStoppedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHDevicePreviewStoppedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHDevicePreviewStoppedEventArgs> tmpEvt = TheRecMHDevicePreviewStoppedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHDeviceCaptureStartFailedEventArgs> TheRecMHDeviceCaptureStartFailedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHDeviceCaptureStartFailedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHDeviceCaptureStartFailedEventArgs> tmpEvt = TheRecMHDeviceCaptureStartFailedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }

        public static event TheRecMHEventHandler<TheRecMHDevicePreviewStartFailedEventArgs> TheRecMHDevicePreviewStartFailedEvent;
        public static void SendTheRecMHEvent(object sender, TheRecMHDevicePreviewStartFailedEventArgs eventArgs)
        {
            TheRecMHEventHandler<TheRecMHDevicePreviewStartFailedEventArgs> tmpEvt = TheRecMHDevicePreviewStartFailedEvent;
            if (tmpEvt != null)
            {
                tmpEvt(sender, eventArgs);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////
    // Graph section

    public class TheRecMHGraphStateChangingEventArgs : TheRecMHEventArgs
    {
        public readonly string DevicePath;
        public readonly string OldState;
        public readonly string NewState;

        public TheRecMHGraphStateChangingEventArgs(string DevicePath, string OldState, string NewState) :
            base(string.Format("Graph State changing from {0} to {1}", OldState, NewState))
        {
            this.DevicePath = DevicePath;
            this.OldState = OldState;
            this.NewState = NewState;
        }
    }

    public class TheRecMHGraphStateChangingFailedEventArgs : TheRecMHEventArgs
    {
        public readonly string DevicePath;
        public readonly string OldState;
        public readonly string NewState;

        public TheRecMHGraphStateChangingFailedEventArgs(string DevicePath, string OldState, string NewState) :
            base(string.Format("Graph State changing from '{0}' to '{1}' failed", OldState, NewState))
        {
            this.DevicePath = DevicePath;
            this.OldState = OldState;
            this.NewState = NewState;
        }
    }

    public class TheRecMHGraphStateChangedEventArgs : TheRecMHEventArgs
    {
        public readonly string DevicePath;
        public readonly string OldState;
        public readonly string NewState;

        public TheRecMHGraphStateChangedEventArgs(string DevicePath, string OldState, string NewState) :
            base(string.Format("Graph State changed from '{0}' to '{1}'", OldState, NewState))
        {

            this.DevicePath = DevicePath;
            this.OldState = OldState;
            this.NewState = NewState;
        }
    }

    ///////////////////////////////////////////////////////////////////////
    // MHRecording section

    public class TheRecMHRecordingCaptureStartEventArgs : TheRecMHEventArgs
    {
        public TheRecMHRecordingCaptureStartEventArgs() :
            base("MH-Recording capture start event.") { }
    }

    public class TheRecMHRecordingCaptureStopEventArgs : TheRecMHEventArgs
    {
        public TheRecMHRecordingCaptureStopEventArgs() :
            base("MH-Recording capture stop event.") { }
    }

    public class TheRecMHRecordingPreviewStartEventArgs : TheRecMHEventArgs
    {
        public TheRecMHRecordingPreviewStartEventArgs() :
            base("MH-Recording preview start event.") { }
    }

    public class TheRecMHRecordingPreviewStopEventArgs : TheRecMHEventArgs
    {
        public TheRecMHRecordingPreviewStopEventArgs() :
            base("MH-Recording preview stop event.") { }
    }

    public class TheRecMHRecordingCaptureStartedEventArgs : TheRecMHEventArgs
    {
        public TheRecMHRecordingCaptureStartedEventArgs() :
            base("MH-Recording capture started event.") { }
    }

    public class TheRecMHRecordingCaptureStoppedEventArgs : TheRecMHEventArgs
    {
        public TheRecMHRecordingCaptureStoppedEventArgs() :
            base("MH-Recording capture stopped event.") { }
    }

    public class TheRecMHRecordingPreviewStartedEventArgs : TheRecMHEventArgs
    {
        public TheRecMHRecordingPreviewStartedEventArgs() :
            base("MH-Recording preview started event.") { }
    }

    public class TheRecMHRecordingPreviewStoppedEventArgs : TheRecMHEventArgs
    {
        public TheRecMHRecordingPreviewStoppedEventArgs() :
            base("MH-Recording preview stopped event.") { }
    }

    public class TheRecMHRecordingCaptureStartFailedEventArgs : TheRecMHEventArgs
    {
        public TheRecMHRecordingCaptureStartFailedEventArgs() :
            base("MH-Recording capture start failed event.") { }
    }

    public class TheRecMHRecordingPreviewStartFailedEventArgs : TheRecMHEventArgs
    {
        public TheRecMHRecordingPreviewStartFailedEventArgs() :
            base("MH-Recording preview start failed event.") { }
    }

    ///////////////////////////////////////////////////////////////////////
    // MHDevice section

    public class TheRecMHDeviceStateChangingEventArgs : TheRecMHEventArgs
    {
        public readonly string OldState;
        public readonly string NewState;
        public readonly MHDevice Device;

        public TheRecMHDeviceStateChangingEventArgs(MHDevice Device, string OldState, string NewState) :
            base(string.Format("'{0}' state changing from {1} to {2}.", Device.FriendlyName, OldState, NewState))
        {
            this.Device = Device;
            this.OldState = OldState;
            this.NewState = NewState;
        }
    }

    public class TheRecMHDeviceStateChangingFailedEventArgs : TheRecMHEventArgs
    {
        public readonly string OldState;
        public readonly string NewState;
        public readonly MHDevice Device;

        public TheRecMHDeviceStateChangingFailedEventArgs(MHDevice Device, string OldState, string NewState) :
            base(string.Format("'{0}' state changing from {1} to {2} failed!", Device.FriendlyName, OldState, NewState))
        {
            this.Device = Device;
            this.OldState = OldState;
            this.NewState = NewState;
        }
    }

    public class TheRecMHDeviceStateChangedEventArgs : TheRecMHEventArgs
    {
        public readonly string OldState;
        public readonly string NewState;
        public readonly MHDevice Device;

        public TheRecMHDeviceStateChangedEventArgs(MHDevice Device, string OldState, string NewState) :
            base(string.Format("'{0}' state changed from {1} to {2}!", Device.FriendlyName, OldState, NewState))
        {
            this.Device = Device;
            this.OldState = OldState;
            this.NewState = NewState;
        }
    }

    public class TheRecMHDeviceCaptureStartedEventArgs : TheRecMHEventArgs
    {
        public readonly MHDevice Device;
        public TheRecMHDeviceCaptureStartedEventArgs(MHDevice Device) :
            base(string.Format("'{0}' started capturing event.", Device.FriendlyName))
        {
            this.Device = Device;
        }
    }

    public class TheRecMHDeviceCaptureStoppedEventArgs : TheRecMHEventArgs
    {
        public readonly MHDevice Device;
        public TheRecMHDeviceCaptureStoppedEventArgs(MHDevice Device) :
            base(string.Format("'{0}' stopped capturing event.", Device.FriendlyName)) 
        {
            this.Device = Device;
        }
    }

    public class TheRecMHDevicePreviewStartedEventArgs : TheRecMHEventArgs
    {
        public readonly MHDevice Device;
        public TheRecMHDevicePreviewStartedEventArgs(MHDevice Device) :
            base(string.Format("'{0}' started preview event.", Device.FriendlyName)) 
        {
            this.Device = Device;
        }
    }

    public class TheRecMHDevicePreviewStoppedEventArgs : TheRecMHEventArgs
    {
        public readonly MHDevice Device;
        public TheRecMHDevicePreviewStoppedEventArgs(MHDevice Device) :
            base(string.Format("'{0}' stopped preview event.", Device.FriendlyName))
        {
            this.Device = Device;
        }
    }

    public class TheRecMHDeviceCaptureStartFailedEventArgs : TheRecMHEventArgs
    {
        public readonly MHDevice Device;
        public TheRecMHDeviceCaptureStartFailedEventArgs(MHDevice Device) :
            base(string.Format("'{0}' starting capture failed event.", Device.FriendlyName)) 
        {
            this.Device = Device;
        }
    }

    public class TheRecMHDevicePreviewStartFailedEventArgs : TheRecMHEventArgs
    {
        public readonly MHDevice Device;
        public TheRecMHDevicePreviewStartFailedEventArgs(MHDevice Device) :
            base(string.Format("'{0}' starting preview failed event.", Device.FriendlyName))
        {
            this.Device = Device;
        }
    }
}
