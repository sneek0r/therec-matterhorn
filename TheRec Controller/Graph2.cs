﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DirectShowLib;
using DirectShowLib.DMO;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;

namespace TheRec_Controller
{
    public abstract class TheRecGraph2 : ITheRecGraph2
    {
        public const int WM_GRAPHNOTIFY = 0x8000 + 1;
        protected const int DEFAULT_FRAMERATE = 25;

        private bool Disposing = false;

        protected IGraphBuilder Graph = new FilterGraph() as IGraphBuilder;
        protected ICaptureGraphBuilder2 GraphBuilder = new CaptureGraphBuilder2() as ICaptureGraphBuilder2;

#if DEBUG
        private DsROTEntry ROTEntry;
#endif

        protected IMediaControl MediaControl { get { return Graph as IMediaControl; } }
        protected IMediaPosition MediaPosition { get { return Graph as IMediaPosition; } }
        protected IMediaEvent MediaEvent { get { return Graph as IMediaEvent; } }
        protected IMediaEventEx MediaEventEx { get { return Graph as IMediaEventEx; } }
        //System.Threading.Thread EventPoller = null;

        Dictionary<String, IVideoWindow> DevicesPreviewWindows = new Dictionary<string, IVideoWindow>();
        Dictionary<String, float> DevicesAspectRatio = new Dictionary<string, float>();


        public TheRecGraph2()
        {
            SetupInterfaces();
        }

        protected void SetupInterfaces()
        {
            int hr = GraphBuilder.SetFiltergraph(Graph);
            DsError.ThrowExceptionForHR(hr);

#if DEBUG
            ROTEntry = new DsROTEntry(Graph);
#endif
        }

        protected virtual void ReleaseInterfaces()
        {
            if (this.Disposing) return;
            this.Disposing = true;

            Stop();

            if (Graph != null)
            {
                Marshal.ReleaseComObject(Graph);
                Graph = null;
            }

            if (GraphBuilder != null)
            {
                Marshal.ReleaseComObject(GraphBuilder);
                Graph = null;
            }

#if DEBUG
            if (ROTEntry != null)
            {
                ROTEntry.Dispose();
                ROTEntry = null;
            }
#endif
            DevicesPreviewWindows.Clear();
            DevicesAspectRatio.Clear();
        }

        public virtual void Dispose()
        {
            ReleaseInterfaces();
            GC.SuppressFinalize(this);
        }

        public virtual void Run()
        {
            int hr;
            
            if (MediaControl != null)
            {
                FilterState OldState = GetFilterState();

                //TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingEventArgs(
                //    FilterStateToString(OldState), FilterStateToString(FilterState.Running)));

                try
                {
                    hr = MediaControl.Run();
                    if (hr == 1)
                    {
                        FilterState PendingState;
                        hr = MediaControl.GetState(5000, out PendingState);
                        if (hr >= 0 && PendingState != FilterState.Running)
                        {
                            throw new Exception("Can't start graph!");
                        }
                    }

                    DsError.ThrowExceptionForHR(hr);
                    //TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangedEventArgs(
                    //    FilterStateToString(OldState), FilterStateToString(FilterState.Running)));

                    //EventPoller = new System.Threading.Thread(this.HandleEvents);
                    //EventPoller.Start();
                }
                catch (Exception e)
                {
                    //TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingFailedEventArgs(
                    //    FilterStateToString(OldState), FilterStateToString(FilterState.Running)));
                    throw e;
                }
            }
        }

        public virtual void Stop()
        {
            int hr;
            if (MediaControl != null)
            {
                FilterState OldState = GetFilterState();

                //TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingEventArgs(
                //    FilterStateToString(OldState), FilterStateToString(FilterState.Stopped)));

                IMediaEvent me = MediaEvent;
                if (me != null)
                {
                    // try to pass the last data to Graph sinks
                    try
                    {
                        hr = MediaControl.StopWhenReady();
                        DsError.ThrowExceptionForHR(hr);

                        FilterState State;
                        do
                        {
                            hr = MediaControl.GetState(Int16.MaxValue, out State);
                            DsError.ThrowExceptionForHR(hr);
                            Debug.WriteLine("Filterstate after StopWhenReady: {0}", State);
                        } while (!FilterState.Stopped.Equals(State));

                        //EventPoller.Interrupt();
                    }
                    catch (COMException e)
                    {
                        Debug.WriteLine("{0}\n{1}", e.Message, e.StackTrace);
                    }
                }

                try
                {
                    hr = MediaControl.Stop();
                    DsError.ThrowExceptionForHR(hr);
                    //TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangedEventArgs(
                    //    FilterStateToString(OldState), FilterStateToString(FilterState.Stopped)));
                }
                catch (Exception e)
                {
                    //TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingFailedEventArgs(
                    //    FilterStateToString(OldState), FilterStateToString(FilterState.Stopped)));
                    throw e;
                }
            }
        }

        public virtual void Pause()
        {
            if (MediaControl != null)
            {
                FilterState OldState = GetFilterState();

                //TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingEventArgs(
                //        FilterStateToString(OldState), FilterStateToString(FilterState.Paused)));

                try
                {
                    int hr = MediaControl.Pause();
                    DsError.ThrowExceptionForHR(hr);
                    //TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangedEventArgs(
                    //    FilterStateToString(OldState), FilterStateToString(FilterState.Paused)));
                }
                catch (Exception e)
                {
                    //TheRecEventSystem.SendTheRecEvent(this, new TheRecGraphStateChangingFailedEventArgs(
                    //    FilterStateToString(OldState), FilterStateToString(FilterState.Paused)));
                    throw e;
                }
            }
        }

        public virtual string GetState()
        {
            return TheRecGraph2.FilterStateToString(this.GetFilterState());
        }

        protected virtual FilterState GetFilterState()
        {
            if (MediaControl != null)
            {
                FilterState State;
                int hr = MediaControl.GetState(5000, out State);
                DsError.ThrowExceptionForHR(hr);

                return State;
            }

            return FilterState.Stopped;
        }

        static string FilterStateToString(FilterState State)
        {
            switch (State)
            {
                case FilterState.Running: return "running";
                case FilterState.Paused: return "paused";
                case FilterState.Stopped: return "stopped";
                default: return "unknown";
            }
        }

        public double GetDuration()
        {
            if (MediaPosition == null) return 0;
            double pos = 0;
            int hr = MediaPosition.get_CurrentPosition(out pos);
            if (hr >= 0)
                return pos;
            else return 0;
        }

        public static DateTime DurationToDateTime(double millisec)
        {
            //return new DateTime((long)((int)millisec * TimeSpan.TicksPerMillisecond)); // convert milli sec to nano sec
            int _millisec = (int)millisec;
            int h = _millisec / 3600;
            int m = (_millisec - (h * 3600)) / 60;
            int s = _millisec - (h * 3600 + m * 60);

            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, h, m, s);
        }

        protected void AddVideoWindowForDevice(String DeviceName, IVideoWindow VideoWindow)
        {
            int hr = VideoWindow.put_AutoShow(OABool.False);
            DsError.ThrowExceptionForHR(hr);

            DevicesPreviewWindows.Add(DeviceName, VideoWindow);
        }

        protected IVideoWindow GetVideoWindowForDevice(String DeviceName)
        {
            if (DevicesPreviewWindows.ContainsKey(DeviceName))
                return DevicesPreviewWindows[DeviceName];
            else return null;
        }

        public void UpdateVideoWindowPosition(String DeviceName, IntPtr VideoWindowOwner, int width, int height)
        {
            //Debug.WriteLine(string.Format("videowindow handle {0}", VideoWindowOwner.ToString()));

            IVideoWindow VideoWindow = GetVideoWindowForDevice(DeviceName);
            if (VideoWindow != null)
            {
                int hr = 0;

                hr = VideoWindow.put_Owner(VideoWindowOwner);
                DsError.ThrowExceptionForHR(hr);

                if (VideoWindowOwner != IntPtr.Zero)
                {
                    hr = VideoWindow.put_WindowStyle(WindowStyle.Child | WindowStyle.ClipSiblings);
                    DsError.ThrowExceptionForHR(hr);

                    VideoWindow.put_Visible(OABool.True);
                }

                int posX = 0, posY = 0;
                float ImageAspectRatio = 4f / 3f;
                if (DevicesAspectRatio.ContainsKey(DeviceName))
                {
                    ImageAspectRatio = DevicesAspectRatio[DeviceName];
                }
                //else
                //{
                //    // fill video window
                //    ImageAspectRatio = (float)width / (float)height;
                //}

                if (ImageAspectRatio > 0)
                {
                    int widthN = Math.Min(width, (int)(height * ImageAspectRatio));
                    int heightN = Math.Min(height, (int)(width / ImageAspectRatio));
                    posX = (width - widthN) / 2;   // Center within control
                    posY = (height - heightN) / 2;
                    width = widthN; height = heightN;
                }

                hr = VideoWindow.SetWindowPosition(posX, posY, width, height);
                DsError.ThrowExceptionForHR(hr);
            }
        }

        protected void SetVideoParms(String DeviceName, ICaptureGraphBuilder2 GraphBuilder, IBaseFilter SourceFilter, int FrameRate, int Width, int Height)
        {
            int hr;
            object o;
            AMMediaType media;
            IAMStreamConfig videoStreamConfig;
            IAMVideoControl videoControl = SourceFilter as IAMVideoControl;
            float ImageAspectRatio;

            // Find the stream config interface
            hr = GraphBuilder.FindInterface(null, MediaType.Video, SourceFilter, typeof(IAMStreamConfig).GUID, out o);

            videoStreamConfig = o as IAMStreamConfig;
            try
            {
                if (videoStreamConfig == null)
                {
                    throw new Exception("Failed to get IAMStreamConfig");
                }

                hr = videoStreamConfig.GetFormat(out media);
                DsError.ThrowExceptionForHR(hr);

                if (media != null)
                {

                    if (media.formatType == FormatType.VideoInfo)
                    {
                        // copy out the videoinfoheader
                        VideoInfoHeader v = new VideoInfoHeader();
                        Marshal.PtrToStructure(media.formatPtr, v);

                        // if overriding the framerate, set the frame rate
                        if (FrameRate > 0)
                        {
                            v.AvgTimePerFrame = 10000000 / FrameRate;
                        }

                        // if overriding the width, set the width
                        if (Width > 0)
                        {
                            v.BmiHeader.Width = Width;
                        }
                        else
                        {
                            Width = v.BmiHeader.Width;
                        }

                        // if overriding the Height, set the Height
                        if (Height > 0)
                        {
                            v.BmiHeader.Height = Height;
                        }
                        else
                        {
                            Height = v.BmiHeader.Height;
                        }

                        // Copy the media structure back
                        Marshal.StructureToPtr(v, media.formatPtr, false);

                        // Set the new format
                        hr = videoStreamConfig.SetFormat(media);
                        DsError.ThrowExceptionForHR(hr);

                        // Format successfully set, calculate image aspect ratio
                        ImageAspectRatio = Math.Abs((float)Width / (float)Height);
                        DevicesAspectRatio.Add(DeviceName, ImageAspectRatio);
                    }
                    else if (media.formatType == FormatType.VideoInfo2)
                    {
                        VideoInfoHeader2 v = new VideoInfoHeader2();
                        Marshal.PtrToStructure(media.formatPtr, v);

                        if (v != null && v.BmiHeader.Height > 0 && v.BmiHeader.Width > 0)
                        {
                            ImageAspectRatio = (float)v.BmiHeader.Height / (float)v.BmiHeader.Width;
                            DevicesAspectRatio.Add(DeviceName, ImageAspectRatio);
                        }
                    }

                DsUtils.FreeAMMediaType(media);
                media = null;
            }

                //// Fix upsidedown video
                //if (videoControl != null)
                //{
                //    VideoControlFlags pCapsFlags;

                //    IPin pPin = DsFindPin.ByCategory(SourceFilter, PinCategory.Capture, 0);
                //    hr = videoControl.GetCaps(pPin, out pCapsFlags);
                //    DsError.ThrowExceptionForHR(hr);

                //    if ((pCapsFlags & VideoControlFlags.FlipVertical) > 0)
                //    {
                //        hr = videoControl.GetMode(pPin, out pCapsFlags);
                //        DsError.ThrowExceptionForHR(hr);

                //        hr = videoControl.SetMode(pPin, 0);
                //    }
                //}
            }
            finally
            {
                if (videoStreamConfig != null) 
                    Marshal.ReleaseComObject(videoStreamConfig);
            }
        }

        protected void HandleEvents()
        {
            IntPtr p1, p2;
            EventCode code;
            try
            {
                while (MediaEventEx != null)
                {
                    if (MediaEventEx.GetEvent(out code, out p1, out p2, int.MaxValue) >= 0)
                    {
                        MediaEventEx.FreeEventParams(code, p1, p2);

                        switch (code)
                        {
                            case EventCode.DeviceLost:
                            case EventCode.StreamBufferWriteFailure:
                            case EventCode.ErrorAbort:
                                Stop();
                                Dispose();
                                return;
                            case EventCode.Complete:
                                return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        protected static IBaseFilter GetFilterFromPath(Guid Category, string Path, string Name)
        {
            object result = null;
            Guid iid = typeof(IBaseFilter).GUID;
            foreach (DsDevice Filter in DsDevice.GetDevicesOfCat(Category))
            {
                if (Filter.DevicePath.CompareTo(Path) == 0 || Filter.Name.CompareTo(Name) == 0)
                {
                    Filter.Mon.BindToObject(null, null, ref iid, out result);
                    break;
                }
            }

            return (IBaseFilter)result;
        }


        public abstract void AddSource(object device);
        public abstract bool IsCaptureGraph();
    }

    public class TheRecPreviewGraph2 : TheRecGraph2
    {
        public override bool IsCaptureGraph() {return false; }

        public override void AddSource(object DeviceObj)
        {
            int hr;

            if (!(DeviceObj is DsDevice))
                throw new Exception();

            DsDevice Device = (DsDevice)DeviceObj;

            IBaseFilter SourceFilter;
            hr = ((IFilterGraph2)Graph).AddSourceFilterForMoniker(Device.Mon, null, Device.Name, out SourceFilter);
            if (hr < 0) throw new Exception();

            IBaseFilter VRenderer = new VideoRendererDefault() as IBaseFilter;
            hr = Graph.AddFilter(VRenderer, "Video Renderer");
            if (hr < 0) throw new Exception();

            hr = GraphBuilder.RenderStream(PinCategory.Preview, MediaType.Video, SourceFilter, null, VRenderer);

            if (hr >= 0)
            {
                IVideoWindow VideoWindow = VRenderer as IVideoWindow;
                hr = VideoWindow.put_AutoShow(OABool.False);
                DsError.ThrowExceptionForHR(hr);

                AddVideoWindowForDevice(Device.Name, VideoWindow);

                try
                {
                    SetVideoParms(Device.Name, GraphBuilder, SourceFilter, DEFAULT_FRAMERATE, -1, -1);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.StackTrace);
                }
            }
            else
            {
                // preview wo audio!!!
                DsError.ThrowExceptionForHR(hr);
            }

            //Marshal.ReleaseComObject(VRenderer);
            Marshal.ReleaseComObject(SourceFilter);
        }
    }

    public class TheRecCaptureMPEGGraph2 : TheRecGraph2
    {
        public override bool IsCaptureGraph() { return true; }
        String CaptureDirectory;

        public TheRecCaptureMPEGGraph2(String CaptureDirectory)
        {
            if (!Directory.Exists(CaptureDirectory))
            {
                Directory.CreateDirectory(CaptureDirectory);
            }
            else
            {
                // directory should be empty!!!
            }

            this.CaptureDirectory = CaptureDirectory;
        }

        public override void AddSource(object DeviceObj)
        {
            AddSource(DeviceObj, null);
        }

        public void AddSource(object DeviceObj, string FileName)
        {
            int hr;

            if (!(DeviceObj is DsDevice))
                throw new Exception("DeviceObj should be of type DsDevice!");

            DsDevice Device = (DsDevice)DeviceObj;
            if (string.IsNullOrEmpty(FileName))
                FileName = Device.Name;

            IBaseFilter SourceFilter;
            hr = ((IFilterGraph2)Graph).AddSourceFilterForMoniker(Device.Mon, null, Device.Name, out SourceFilter);
            if (hr < 0) DsError.ThrowExceptionForHR(hr);

            IBaseFilter Encoder = GetFilterFromPath(FilterCategory.MediaMultiplexerCategory, null, @"Microsoft MPEG-2 Encoder");

            hr = Graph.AddFilter(Encoder, "MPEG Encoder");
            DsError.ThrowExceptionForHR(hr);

            try
            {
                LinkDVSource(Device.Name, SourceFilter, Encoder, Encoder, null);
            }
            catch (Exception)
            {
                bool linked = false;
                try
                {
                    LinkAudioSource(SourceFilter, Encoder, null);
                    linked = true;
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.StackTrace);
                }

                try
                {
                    LinkVideoSource(Device.Name, SourceFilter, Encoder, null);
                    linked = true;
                }
                catch (Exception e) 
                {
                    Debug.WriteLine(e.StackTrace);
                }

                if (!linked) throw new Exception();
            }

            string FileNameTmp = Path.Combine(CaptureDirectory, FileName + ".mpg");
            int FileNamePostfix = 0;
            while (File.Exists(FileNameTmp))
            {
                FileNamePostfix++;
                FileNameTmp = Path.Combine(CaptureDirectory, FileName + "_" + FileNamePostfix + ".mpg");
            }

            if (FileNamePostfix > 0)
            {
                TheRecEventSystem.SendTheRecEvent(this, new TheRecOutputFileNameChangedEventArgs(Device.Name, FileName + ".mpg", FileNameTmp));
            }

            IFileSinkFilter FileSink = new FileWriter() as IFileSinkFilter;
            FileSink.SetFileName(FileNameTmp, null);
            Graph.AddFilter(FileSink as IBaseFilter, "File Sink");

            hr = GraphBuilder.RenderStream(null, null, Encoder, null, FileSink as IBaseFilter);
            DsError.ThrowExceptionForHR(hr);
        }

        protected void LinkDVSource(String DeviceName, IBaseFilter SourceFilter, IBaseFilter AudioEncoder, IBaseFilter VideoEncoder, IBaseFilter Mux)
        {
            int hr;
            IBaseFilter DVSplitter = new DVSplitter() as IBaseFilter;
            hr = Graph.AddFilter(DVSplitter, "DV Splitter");
            if (hr < 0) throw new Exception();
            
            hr = GraphBuilder.RenderStream(PinCategory.Capture, MediaType.Interleaved, SourceFilter, DVSplitter, VideoEncoder);
            if (hr < 0)
            {
                Graph.RemoveFilter(DVSplitter);
                DsError.ThrowExceptionForHR(hr);
            }

            if (Mux != null)
            {
                hr = GraphBuilder.RenderStream(null, null, VideoEncoder, null, Mux);
                DsError.ThrowExceptionForHR(hr);
            }

            hr = GraphBuilder.RenderStream(null, null, DVSplitter, null, AudioEncoder);
            if (hr < 0)
            {
                Graph.RemoveFilter(DVSplitter);
                DsError.ThrowExceptionForHR(hr);
            }

            if (AudioEncoder != VideoEncoder)
            {
                hr = GraphBuilder.RenderStream(null, null, AudioEncoder, null, Mux);
                DsError.ThrowExceptionForHR(hr);
            }

            IBaseFilter VideoRenderer = new VideoRendererDefault() as IBaseFilter;
            hr = Graph.AddFilter(VideoRenderer, "Video Renderer");

            hr = GraphBuilder.RenderStream(PinCategory.Preview, MediaType.Interleaved, SourceFilter, null, VideoRenderer);
            if (hr < 0)
            {
                Graph.RemoveFilter(DVSplitter);
                Graph.RemoveFilter(VideoRenderer);
                DsError.ThrowExceptionForHR(hr);
            }
            else
            {
                IVideoWindow VideoWindow = VideoRenderer as IVideoWindow;
                AddVideoWindowForDevice(DeviceName, VideoWindow);

                try
                {
                    SetVideoParms(DeviceName, GraphBuilder, SourceFilter, -1, -1, -1);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.StackTrace);
                }
            }
        }

        protected void LinkAudioSource(IBaseFilter SourceFilter, IBaseFilter AudioEncoder, IBaseFilter Mux)
        {
            int hr;
            hr = GraphBuilder.RenderStream(null, MediaType.Audio, SourceFilter, null, AudioEncoder);
            if (hr < 0) DsError.ThrowExceptionForHR(hr);
            if (Mux != null)
            {
                hr = GraphBuilder.RenderStream(null, null, AudioEncoder, null, Mux);
                DsError.ThrowExceptionForHR(hr);
            }
        }

        protected void LinkVideoSource(String DeviceName, IBaseFilter SourceFilter, IBaseFilter VideoEncoder, IBaseFilter Mux)
        {
            int hr;
            //IDMOWrapperFilter DMOFrameRateConverter = new DMOWrapperFilter() as IDMOWrapperFilter;
            //hr = DMOFrameRateConverter.Init(new Guid("{01F36CE2-0907-4D8B-979D-F151BE91C883}"), DMOCategory.VideoEffect);
            //DMOError.ThrowExceptionForHR(hr);
            //hr = Graph.AddFilter(DMOFrameRateConverter as IBaseFilter, "Framerate Converter");
            //DMOError.ThrowExceptionForHR(hr);

            //hr = GraphBuilder.RenderStream(PinCategory.Capture, MediaType.Video, SourceFilter, null, DMOFrameRateConverter as IBaseFilter);
            //DsError.ThrowExceptionForHR(hr);
            //// setup framerate on DMO
            ////IPropertyStore propstore = DMOFrameRateConverter as IPropertyStore;
            ////if (propstore != null)
            ////{
            ////    uint Count;
            ////    if (propstore.GetCount(out Count) >= 0)
            ////    {
            ////        Debug.WriteLine("DMO has {0} properties", Count);

            ////        for (uint i = 0; i < Count; i++)
            ////        {
            ////            PROPERTYKEY propkey;
            ////            if (propstore.GetAt(i, out propkey) >= 0)
            ////            {
            ////                Debug.WriteLine("propkey.fmtid: {0}, propkey.pid: {1}", new Object[] { propkey.fmtid, propkey.pid.ToString() });

            ////                PropVariant propv;
            ////                if (propstore.GetValue(ref propkey, out propv) >= 0)
            ////                {
            ////                    Debug.WriteLine("propv.type: {0}, propv.value: {1}/{2}", propv.variantType, propv.value.numerator, propv.value.denumerator);
            ////                    if (propkey.pid.ToUInt32() == 2 && propv.variantType == 21)
            ////                    {
            ////                        propv.value.numerator = 25u;
            ////                        propv.value.denumerator = 1u;

            ////                        if (propstore.SetValue(ref propkey, ref propv) >= 0)
            ////                        {
            ////                            Debug.WriteLine("juhu");

            ////                            propstore.GetValue(ref propkey, out propv);
            ////                            Debug.WriteLine("propv.type: {0}, propv.value: {1}/{2}", propv.variantType, propv.value.numerator, propv.value.denumerator);
            ////                        }
            ////                    }
            ////                }
            ////            }
            ////        }
            ////    }
            ////}
            //hr = GraphBuilder.RenderStream(null, MediaType.Video, DMOFrameRateConverter as IBaseFilter, null, VideoEncoder);

            //hr = GraphBuilder.RenderStream(PinCategory.Capture, MediaType.Video, SourceFilter, DMOFrameRateConverter as IBaseFilter, VideoEncoder);

            hr = GraphBuilder.RenderStream(PinCategory.Capture, MediaType.Video, SourceFilter, null, VideoEncoder);
            //if (hr < 0)
            //{
            //    Graph.RemoveFilter(DMOFrameRateConverter as IBaseFilter);
            DsError.ThrowExceptionForHR(hr);
            //}

            if (Mux != null)
            {
                hr = GraphBuilder.RenderStream(null, null, VideoEncoder, null, Mux);
                DsError.ThrowExceptionForHR(hr);
            }

            IBaseFilter VideoRenderer = new VideoRendererDefault() as IBaseFilter;
            hr = Graph.AddFilter(VideoRenderer, "Video Renderer");
            if (hr < 0) DsError.ThrowExceptionForHR(hr);

            hr = GraphBuilder.RenderStream(PinCategory.Preview, MediaType.Video, SourceFilter, null, VideoRenderer);
            if (hr < 0)
            {
                Graph.RemoveFilter(VideoRenderer);
                DsError.ThrowExceptionForHR(hr);
            }
            else
            {
                IVideoWindow VideoWindow = VideoRenderer as IVideoWindow;
                AddVideoWindowForDevice(DeviceName, VideoWindow);

                try
                {
                    SetVideoParms(DeviceName, GraphBuilder, SourceFilter, DEFAULT_FRAMERATE, -1, -1);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.StackTrace);
                }
            }
        }
    }
}
