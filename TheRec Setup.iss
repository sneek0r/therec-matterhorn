
#define APP_NAME 'TheRec Matterhorn'

[Setup]
AppName={#APP_NAME}
AppVersion=1.0.13      
AppPublisher=VirtUOS
AppPublisherURL=http://zentrum.virtuos.uos.de/
AppSupportURL=http://zentrum.virtuos.uos.de/therec/
AppComments=Multimedia Capture Software
DefaultDirName={pf}\{#APP_NAME}
DefaultGroupName=
OutputDir=Setup
UninstallDisplayIcon={app}\TheRec_Matterhorn.exe,0
UninstallDisplayName={#APP_NAME}

[Dirs]
Name: "{app}\HelpFiles"
Name: "{app}\HelpFiles\files"
Name: "{app}\HelpFiles\img"

[Files]
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\bin\x86\Release\TheRec_Matterhorn.exe"; DestDir: "{app}"; Flags: 32bit
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\bin\x86\Release\TheRec Controller.dll"; DestDir: "{app}"; Flags: 32bit
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\bin\x86\Release\TheRec_Matterhorn_Utils.dll"; DestDir: "{app}"; Flags: 32bit
Source: "D:\workspace\TheRec\v3\libs\DirectShowLib-2005.dll"; DestDir: "{app}"; Flags: 32bit
Source: "D:\workspace\TheRec\v3\License.txt"; DestDir: "{app}"
Source: "D:\workspace\TheRec\v3\License-de.txt"; DestDir: "{app}"
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\HelpFiles\index.html"; DestDir: "{app}\HelpFiles"; Flags: ignoreversion
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\HelpFiles\jquery.scrollTo-min.js"; DestDir: "{app}\HelpFiles"; Flags: ignoreversion
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\HelpFiles\style.css"; DestDir: "{app}\HelpFiles"; Flags: ignoreversion
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\HelpFiles\files\checklist_de.pdf"; DestDir: "{app}\HelpFiles\files"; Flags: ignoreversion
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\HelpFiles\files\checklist_en.pdf"; DestDir: "{app}\HelpFiles\files"; Flags: ignoreversion
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\HelpFiles\img\check.png"; DestDir: "{app}\HelpFiles\img"; Flags: ignoreversion
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\HelpFiles\img\options.png"; DestDir: "{app}\HelpFiles\img"; Flags: ignoreversion
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\HelpFiles\img\start.png"; DestDir: "{app}\HelpFiles\img"; Flags: ignoreversion
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\HelpFiles\img\the-REC1.png"; DestDir: "{app}\HelpFiles\img"; Flags: ignoreversion
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\HelpFiles\img\therec.png"; DestDir: "{app}\HelpFiles\img"; Flags: ignoreversion
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\HelpFiles\img\ul1.png"; DestDir: "{app}\HelpFiles\img"; Flags: ignoreversion
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\HelpFiles\img\ul2.png"; DestDir: "{app}\HelpFiles\img"; Flags: ignoreversion
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\HelpFiles\img\ul3.png"; DestDir: "{app}\HelpFiles\img"; Flags: ignoreversion
Source: "D:\workspace\TheRec\v3\TheRec Matterhorn\HelpFiles\img\ul_ok.png"; DestDir: "{app}\HelpFiles\img"; Flags: ignoreversion

[Icons]
Name: "{userdesktop}\{#APP_NAME}"; Filename: "{app}\TheRec_Matterhorn.exe"; WorkingDir: "{app}"; IconFilename: "{app}\TheRec_Matterhorn.exe"; IconIndex: 0
Name: "{group}\{#APP_NAME}"; Filename: "{app}\TheRec_Matterhorn.exe"; WorkingDir: "{app}"; IconFilename: "{app}\TheRec_Matterhorn.exe"; IconIndex: 0
Name: "{group}\TheRec Help"; Filename: "{app}\HelpFiles\index.html"; WorkingDir: "{app}\HelpFiles"

[Languages]
Name: "en"; MessagesFile: "compiler:Default.isl"; LicenseFile: "License.txt"
Name: "de"; MessagesFile: "compiler:Languages\German.isl"; LicenseFile: "License-de.txt"

[Code]
{ 
  Preinstall code.
  Remove old TheRec version installed with InstallShield installer.
}
function PrepareToInstall(var NeedsRestart: Boolean): String;
var
  ResultCode: Integer;
begin
  Exec('MsiExec.exe', '/quiet /x{CAF11769-D54A-46CD-8310-144425CD886C}', '', 
    SW_SHOW, ewWaitUntilTerminated, ResultCode);
end;
