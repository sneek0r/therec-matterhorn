﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.ComponentModel;


namespace TheRec_Matterhorn_Utils
{
    [Serializable()]
    public class MHOptions : INotifyPropertyChanged
    {
        [NonSerialized()]
        readonly string OPTIONS_FILE_NAME = @"config.dat";
        [NonSerialized()]
        readonly string OPTIONS_FILE_PATH = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "TheRec Matterhorn");

        public string CaptureOutputPath { get { return _CaptureOutputPath; } set { _CaptureOutputPath = Path.GetFullPath(value); UpdateProperty("CaptureOutputPath"); } }
        private string _CaptureOutputPath = "";

        public MHDevice AudioEncoder { get; set; }
        public MHDevice VideoEncoder { get; set; }

        public ObservableCollection<MHDevice> AudioEncoders { get { return _AudioEncoders; } }
        [NonSerialized()]
        private ObservableCollection<MHDevice> _AudioEncoders;

        public ObservableCollection<MHDevice> VideoEncoders { get { return _VideoEncoders; } }
        [NonSerialized()]
        private ObservableCollection<MHDevice> _VideoEncoders;

        public ObservableCollection<MHDevice> Devices { get { return _Devices; } }
        private ObservableCollection<MHDevice> _Devices;

        public MHOptions() : this(false) { }

        public MHOptions(bool OnlyEnabledDevices = false)
        {
            _AudioEncoders = new ObservableCollection<MHDevice>(MHUtils.GetAudioCompressors());
            _VideoEncoders = new ObservableCollection<MHDevice>(MHUtils.GetVideoCompressors());
            try
            {
                this.LoadOptionsFromFile();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                
                _Devices = new ObservableCollection<MHDevice>(MHUtils.GetAllDevices());
            }

            // list only connected (and enabled) devices..
            ObservableCollection<MHDevice> enabledDevices = null;
            List<MHDevice> AllDevices = MHUtils.GetAllDevices();
            if (AllDevices.Count == 0)
            {
                Devices.Clear();
                enabledDevices = new ObservableCollection<MHDevice>();
                return;
            }

            if (OnlyEnabledDevices)
            {
                enabledDevices = new ObservableCollection<MHDevice>(
                    Devices.Where(dev => dev.Enabled && AllDevices.Contains(dev)).OrderBy(dev => dev.FriendlyName));
            }
            else
            {
                enabledDevices = new ObservableCollection<MHDevice>(
                    Devices.Where(dev => AllDevices.Contains(dev)).Union(AllDevices.Where(dev => !Devices.Contains(dev))).OrderBy(dev => dev.Name));
            }

            _Devices = enabledDevices;
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        public void UpdateProperty(string PropertyName)
        {
            PropertyChangedEventHandler PropertyChanged = this.PropertyChanged;
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }

        public void SaveOptionsToFile()
        {
            // create config directory
            if (!Directory.Exists(OPTIONS_FILE_PATH))
            {
                Directory.CreateDirectory(OPTIONS_FILE_PATH);
            }

            // create media output directory
            if (!Directory.Exists(CaptureOutputPath))
            {
                try
                {
                    Directory.CreateDirectory(CaptureOutputPath);
                    Directory.CreateDirectory(Path.Combine(CaptureOutputPath, MHRecording.DEFAULT_SERIES_NAME));
                }
                catch (Exception ex)
                {
                    throw new Exception("Can't create the capture output directory,\nplease chose another!");
                }
            }

            using (FileStream fs = new FileStream(Path.Combine(OPTIONS_FILE_PATH, OPTIONS_FILE_NAME), FileMode.Create))
            {
                try
                {
                    Version Version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                    string VersionStr = string.Format("{0:0}.{1:00}.{2:0000}.{3:0000}", Version.Major, Version.Minor, Version.Build, Version.Revision);
                    File.WriteAllText(Path.Combine(OPTIONS_FILE_PATH, OPTIONS_FILE_NAME + ".ver"), VersionStr, Encoding.Default);


                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(fs, this);
                }
                catch (Exception ex)
                {
                    throw new Exception("Can not write config file!");
                }
            }
        }

        public void LoadOptionsFromFile()
        {
            MHOptions options;
            using (FileStream fs = new FileStream(Path.Combine(OPTIONS_FILE_PATH, OPTIONS_FILE_NAME), FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                options = (MHOptions)formatter.Deserialize(fs);
            }

            this.CaptureOutputPath = options.CaptureOutputPath;
            this.AudioEncoder = options.AudioEncoder;
            this.VideoEncoder = options.VideoEncoder;
            this._Devices = options._Devices;
        }
    }
}
