﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TheRec_Matterhorn_Utils;

namespace TheRec_Matterhorn
{
    /// <summary>
    /// Interaktionslogik für OptionsWindow.xaml
    /// </summary>
    public partial class OptionsWindow : Window
    {
        public OptionsWindow()
        {
            InitializeComponent();
        }

        private void DebugBtnClicked(object sender, RoutedEventArgs e)
        {
            MHOptions Options = (MHOptions)FindResource("OptionsModel");
            Console.WriteLine(string.Format("CaptureOutputPath: {0}\nselected AudioEncoder: {1}\nselected VideoEncoder: {2}\n{3}",
                Options.CaptureOutputPath, Options.AudioEncoder.Name, Options.VideoEncoder.Name,
                printDevices(Options.Devices.ToList<MHDevice>())));
        }

        private string printDevices(List<MHDevice> Devices)
        {
            string result = "";
            foreach (MHDevice device in Devices)
            {
                result += string.Format("======================\n{0}:\nenabled: {2}\nfriendly name: {1}\nflavour: {3}\n\n", 
                    device.Name, device.FriendlyName, device.Enabled, device.Flavour);
            }
            return result;
        }

        private void SaveBtn_Clicked(object sender, RoutedEventArgs e)
        {
            MHOptions Options = (MHOptions)FindResource("OptionsModel");
            try
            {
                Options.SaveOptionsToFile();
                this.DialogResult = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show(this, ex.Message, "Error saving file", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SelectFoolderButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog FolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            FolderDialog.Description = "Select the base media output directory";
            FolderDialog.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);
            FolderDialog.ShowNewFolderButton = true;
            System.Windows.Forms.DialogResult result = FolderDialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                MHOptions Options = (MHOptions)FindResource("OptionsModel");
                if (Options != null)
                {
                    Options.CaptureOutputPath = FolderDialog.SelectedPath;
                }
            }
        }
    }
}
