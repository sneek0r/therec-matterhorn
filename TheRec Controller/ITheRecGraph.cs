﻿using System;

namespace TheRec_Controller
{
    public interface ITheRecGraph
    {
        void BuildGraph();
        void HandleGraphEvent();
        void Pause();
        void Run();
        void Stop();
        string GetState();
    }

    public interface ITheRecGraph2 : IDisposable
    {
        bool IsCaptureGraph();
        void Pause();
        void Run();
        void Stop();
        string GetState();
        double GetDuration();
        void UpdateVideoWindowPosition(String DeviceName, IntPtr VideoWindowOwner, int width, int height);

        void AddSource(object device);
    }

    public interface ITheRecGraph3 : IDisposable
    {
        bool IsCaptureGraph();
        void Pause();
        void Run();
        void Stop();
        string GetState();
        double GetDuration();
        void AddSource(string DevicePath, string FriendlyName, string Flavour, string OutputPath);
        void HandleGraphEvent();
        void UpdateVideoWindowPosition(IntPtr VideoWindowOwner, int width, int height);
    }
}
