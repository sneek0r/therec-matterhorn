﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.ComponentModel;
using TheRec_Controller;
using DirectShowLib;
using System.Threading;
using System.Threading.Tasks;

using System.Xml;
using System.Xml.Serialization;

namespace TheRec_Matterhorn_Utils
{
    public class MHRecording : IDisposable, INotifyPropertyChanged
    {
        public const string STATE_IDLE = "idle";
        public const string STATE_CAPTURING = "capturing";
        public const string STATE_STARTING = "starting";
        public const string STATE_STOPPING = "stopping";
        public const string STATE_CAPTURE_START_FAILED = "capture start failed";

        public const string DEFAULT_SERIES_NAME = "Default";

        public string Title { get { return _Title; } set { _Title = value; _CaptureOutputPath = null; UpdateProperty("Title"); } }
        private string _Title;

        public string Series { get { return _Series; } set { _Series = value; Console.WriteLine(string.Format("Serie: {0}", value));  UpdateProperty("Series"); } }
        private string _Series;

        public string Presenter { get { return _Presenter; } set { _Presenter = value; UpdateProperty("Presenter"); } }
        private string _Presenter;
        public DateTime DateTime { get { return _DateTime; } set { _DateTime = value; UpdateProperty("DateTime"); } }
        private DateTime _DateTime;
        public Int32 DateTimeHour { get { return _DateTimeHour; } set { _DateTimeHour = value; UpdateProperty("DateTimeHour"); } }
        private Int32 _DateTimeHour;
        public Int32 DateTimeMinute { get { return _DateTimeMinute; } set { _DateTimeMinute = value; UpdateProperty("DateTimeMinute"); } }
        private Int32 _DateTimeMinute;
        public string Description { get { return _Description; } set { _Description = value; UpdateProperty("Description"); } }
        private string _Description;

        public bool Editable { get { return State == STATE_IDLE; } }
        public string State { get { return _State; } private set { _State = value; UpdateProperty("State"); UpdateProperty("Editable"); } }
        private string _State;
        //public DateTime CaptureDuration { get { return _CaptureDuration; } protected set { _CaptureDuration = value; UpdateProperty("CaptureDuration"); } }
        //protected DateTime _CaptureDuration = new DateTime();
        public bool IsCapturing { get { return OptionsModel.Devices.Where(dev => dev.Capturing).Count() > 0; } }

        public DateTime CaptureDuration { 
            get 
            {
                return new DateTime(DateTime.Now.Ticks - _CaptureStart); 
            } 
            protected set {  UpdateProperty("CaptureDuration"); } }
        private long _CaptureStart = DateTime.Now.Ticks;

        public MHOptions OptionsModel { get; set; }
        
        public string CaptureOutputPath
        {
            get
            {
                lock (_CaptureOutputPathLock) {
                    if (string.IsNullOrEmpty(_CaptureOutputPath))
                    {
                        string OutputPath;
                        if (string.IsNullOrEmpty(Series))
                        {
                            OutputPath = Path.Combine(OptionsModel.CaptureOutputPath, DEFAULT_SERIES_NAME);
                        }
                        else
                        {
                            OutputPath = Path.Combine(OptionsModel.CaptureOutputPath, Series);
                        }

                        if (Directory.Exists(Path.Combine(OutputPath, Title)))
                        {
                            int i;
                            for (i = 1; Directory.Exists(Path.Combine(OutputPath, Title + "_" + i)); i++) ;
                            OutputPath = Path.Combine(OutputPath, Title + "_" + i);
                        }
                        else
                        {
                            OutputPath = Path.Combine(OutputPath, Title);
                        }
                        _CaptureOutputPath = OutputPath;
                    }
                }
                return _CaptureOutputPath;
            }
        }
        private string _CaptureOutputPath = null;
        private object _CaptureOutputPathLock = new object();
        
        public MHRecording()
        {
            Series = DEFAULT_SERIES_NAME;
            Title = "Recording";
            Presenter = "";
            Description = "";
            DateTime = DateTime.Now;
            DateTimeHour = DateTime.Hour;
            DateTimeMinute = DateTime.Minute / 5 * 5;
            OptionsModel = new MHOptions(OnlyEnabledDevices: true);
            _State = STATE_IDLE;
            
            MHEventSystem.TheRecMHRecordingCaptureStartEvent += new TheRecMHEventHandler<TheRecMHRecordingCaptureStartEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingCaptureStopEvent += new TheRecMHEventHandler<TheRecMHRecordingCaptureStopEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingPreviewStartEvent += new TheRecMHEventHandler<TheRecMHRecordingPreviewStartEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingPreviewStopEvent += new TheRecMHEventHandler<TheRecMHRecordingPreviewStopEventArgs>(this.OnTheRecMHEvent);

            MHEventSystem.TheRecMHRecordingCaptureStartedEvent += new TheRecMHEventHandler<TheRecMHRecordingCaptureStartedEventArgs>(this.OnTheRecMHEvent);
        }

        public void Dispose()
        {
            MHEventSystem.TheRecMHRecordingCaptureStartEvent -= new TheRecMHEventHandler<TheRecMHRecordingCaptureStartEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingCaptureStopEvent -= new TheRecMHEventHandler<TheRecMHRecordingCaptureStopEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingPreviewStartEvent -= new TheRecMHEventHandler<TheRecMHRecordingPreviewStartEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingPreviewStopEvent -= new TheRecMHEventHandler<TheRecMHRecordingPreviewStopEventArgs>(this.OnTheRecMHEvent);

            MHEventSystem.TheRecMHRecordingCaptureStartedEvent -= new TheRecMHEventHandler<TheRecMHRecordingCaptureStartedEventArgs>(this.OnTheRecMHEvent);

            StopGraph();
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void UpdateProperty(string PropertyName)
        {
            PropertyChangedEventHandler PropertyChanged = this.PropertyChanged;
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }

        public void StopGraph()
        {
            bool captured = IsCapturing;
            if (captured)
            {
                State = STATE_STOPPING;
                lock (_CaptureOutputPathLock)
                {
                    _CaptureOutputPath = null;
                }
            }

            foreach (MHDevice Device in OptionsModel.Devices)
            {
                Device.StopGraph();
            }
            State = STATE_IDLE;

            if (captured)
                MHEventSystem.SendTheRecMHEvent(this, new TheRecMHRecordingCaptureStoppedEventArgs());
            else
                MHEventSystem.SendTheRecMHEvent(this, new TheRecMHRecordingPreviewStoppedEventArgs());
        }

        public void StartPreview()
        {
            StopGraph();

            IEnumerable<MHDevice> Devices = OptionsModel.Devices.Where(dev => dev.IsVideo);
            foreach (MHDevice Device in Devices)
            {
                try
                {
                    Device.BuildPreviewGraph();
                    Device.RunGraph();
                    MHEventSystem.SendTheRecMHEvent(this, new TheRecMHDevicePreviewStartedEventArgs(Device));
                }
                catch (Exception e)
                {
                    MHEventSystem.SendTheRecMHEvent(this, new TheRecMHDevicePreviewStartFailedEventArgs(Device));
                }
            }
            MHEventSystem.SendTheRecMHEvent(this, new TheRecMHRecordingPreviewStartedEventArgs());
        }

        public void StartCapture()
        {
            StopGraph();

            State = STATE_STARTING;
            IEnumerable<MHDevice> Devices = OptionsModel.Devices.Where(dev => dev.Active);
            List<MHDevice> ReadyDevices = new List<MHDevice>();
            string CaptureOutputPath = this.CaptureOutputPath;
            foreach (MHDevice Device in Devices)
            {
                try
                {
                    Device.BuildCaptureGraph(CaptureOutputPath);
                    ReadyDevices.Add(Device);
                }
                catch (Exception e)
                {
                    MHDebug.WriteLine(e.Message);
                    //TODO send failed event
                }
            }

            int DevicesStarted = 0;
            foreach (MHDevice Device in ReadyDevices)
            {
                try
                {
                    Device.RunGraph();
                    DevicesStarted++;
                    MHEventSystem.SendTheRecMHEvent(this, new TheRecMHDeviceCaptureStartedEventArgs(Device));
                }
                catch (Exception e)
                {
                    MHDebug.WriteLine(e.Message);
                    MHEventSystem.SendTheRecMHEvent(this, new TheRecMHDeviceCaptureStartFailedEventArgs(Device));
                }
            }
            if (DevicesStarted < 1)
            {
                State = STATE_CAPTURE_START_FAILED;
                lock (_CaptureOutputPathLock)
                {
                    _CaptureOutputPath = null;
                }
                MHEventSystem.SendTheRecMHEvent(this, new TheRecMHRecordingCaptureStartFailedEventArgs());
            }
            else
            {
                State = STATE_CAPTURING;
                MHEventSystem.SendTheRecMHEvent(this, new TheRecMHRecordingCaptureStartedEventArgs());
                new Thread(this.UpdateGraphRunningTime).Start();
            }
        }

        //protected void UpdateGraphRunningTime()
        //{
        //    IEnumerable<MHDevice> Devices = OptionsModel.Devices.Where(dev => dev.Capturing);
        //    if (Devices.Count() > 0)
        //    {
        //        MHDevice Device = Devices.First<MHDevice>();
        //        while (Device.Capturing)
        //        {
        //            CaptureDuration = Device.GetDuration();
        //            Thread.Sleep(1000);
        //        }
        //    }
        //}

        protected void UpdateGraphRunningTime()
        {
            _CaptureStart = DateTime.Now.Ticks;
            while (IsCapturing)
            {
                CaptureDuration = new DateTime(DateTime.Now.Ticks - _CaptureStart);
                Thread.Sleep(1000);
            }
        }

        public void OnTheRecEvent(object sender, TheRecEventArgs eventArgs)
        {
            
        }

        public void OnTheRecMHEvent(object sender, TheRecMHEventArgs eventArgs)
        {
            if (eventArgs is TheRecMHRecordingCaptureStartEventArgs)
            {
                StartCapture();
            }
            else if (eventArgs is TheRecMHRecordingPreviewStartEventArgs)
            {
                StartPreview();
            }
            else if (eventArgs is TheRecMHRecordingCaptureStopEventArgs
                || eventArgs is TheRecMHRecordingPreviewStopEventArgs)
            {
                StopGraph();
            }
            else if (eventArgs is TheRecMHRecordingCaptureStartedEventArgs)
            {
                MHRecordingSerializer.Serialize(this);
            }
        }
    }

    [XmlRoot(ElementName="dublincore", Namespace="http://www.opencastproject.org/xsd/1.0/dublincore/")]
    public class MHRecordingSerializer
    {
        [XmlElement("title", Namespace = "http://purl.org/dc/terms/")]
        public string Title;

        [XmlElement("creator", Namespace = "http://purl.org/dc/terms/")]
        public string Author;

        [XmlElement("created", Namespace="http://purl.org/dc/terms/")]
        public DateTime Created;

        [XmlElement("description", Namespace="http://purl.org/dc/terms/", IsNullable=false)]
        public string Description;

        public MHRecordingSerializer() { }

        MHRecordingSerializer(MHRecording Recording)
        {
            this.Author = Recording.Presenter;
            this.Created = System.TimeZone.CurrentTimeZone.ToUniversalTime(
                new DateTime(Recording.DateTime.Year, Recording.DateTime.Month, 
                Recording.DateTime.Day, Recording.DateTimeHour, Recording.DateTimeMinute, 0));
            this.Description = Recording.Description;
            this.Title = Recording.Title;
        }

        public static void Serialize(MHRecording Recording) 
        {
            MHRecordingSerializer Data = new MHRecordingSerializer(Recording);
            
            if (!Directory.Exists(Recording.CaptureOutputPath))
            {
                Debug.WriteLine("Can not serialize recording, because outputpath does not exist!");
                return;
            }

            using (TextWriter Writer = new StreamWriter(
                Path.Combine(Recording.CaptureOutputPath, "episode.xml")))
            {

                XmlSerializerNamespaces Namespaces = new XmlSerializerNamespaces();
                Namespaces.Add("dcterms", "http://purl.org/dc/terms/");

                XmlSerializer Serializer = new XmlSerializer(typeof(MHRecordingSerializer));
                Serializer.Serialize(Writer, Data, Namespaces);
            }
        }
    }
}
