﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TheRec_Matterhorn_Utils;
using System.Diagnostics;
using Microsoft.Win32;
using System.IO;

namespace TheRec_Matterhorn
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static MainWindow Instance { get { return _Instance; } }
        private static MainWindow _Instance = null;

        private Boolean WantPreview = false;

        public MainWindow()
        {
            InitializeComponent();
            MainWindow._Instance = this;
            this.DataContext = new MHRecording();

            MHEventSystem.TheRecMHRecordingCaptureStoppedEvent += new TheRecMHEventHandler<TheRecMHRecordingCaptureStoppedEventArgs>(this.OnTheRecMHEvent);
            MHEventSystem.TheRecMHRecordingCaptureStartFailedEvent += new TheRecMHEventHandler<TheRecMHRecordingCaptureStartFailedEventArgs>(this.OnTheRecMHEvent);
        }

        public void OnTheRecMHEvent(object sender, TheRecMHEventArgs eventArgs)
        {
            if (eventArgs is TheRecMHRecordingCaptureStoppedEventArgs && WantPreview)
            {
                MHEventSystem.SendTheRecMHEvent(this, new TheRecMHRecordingPreviewStartEventArgs());
            }
            else if (eventArgs is TheRecMHRecordingCaptureStartFailedEventArgs)
            {
                MessageBox.Show(this, "Capture not started! Are all devices you choose available?", "Capture start failed!", 
                    MessageBoxButton.OK, MessageBoxImage.Error);
                MHEventSystem.SendTheRecMHEvent(this, new TheRecMHRecordingPreviewStartEventArgs());
            }
        }

        public void StartCapture() 
        {
            MHEventSystem.SendTheRecMHEvent(this, new TheRecMHRecordingCaptureStartEventArgs());
        }

        public void StopCapture(bool StartPreview = true)
        {
            WantPreview = StartPreview;
            MHEventSystem.SendTheRecMHEvent(this, new TheRecMHRecordingCaptureStopEventArgs());
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MHRecording Recording = DataContext as MHRecording;
            if (Recording == null) return;

            if (!Recording.State.Equals(MHRecording.STATE_IDLE))
            {
                MessageBoxResult choice = MessageBox.Show(this, "An recording is running, do you want to stop it?",
                    "TheRec Matterhorn Quit?", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
                switch (choice)
                {
                    case MessageBoxResult.Yes:
                        StopCapture(StartPreview: false);
                        break;
                    default: e.Cancel = true;
                        return;
                }
            }
            else
            {
                Recording.StopGraph();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            MHEventSystem.SendTheRecMHEvent(this, new TheRecMHRecordingPreviewStartEventArgs());
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (sender == QuitMenuItem) {
                this.Close();
            }
            else if (sender == OptionsMenuItem)
            {
                ShowOptoionsDialog();
            }
            else if (sender == AboutMenuItem)
            {
                AboutDialog About = new AboutDialog();
                About.Owner = this;
                About.ShowDialog();
            }
            //else if (sender == HelpMenuItem)
            //{
            //    string AppDir = Directory.GetCurrentDirectory();
            //    string key = @"http\shell\open\command";
            //    string BrowserPath = null;

            //    try
            //    {
            //        if (Registry.ClassesRoot.OpenSubKey(key, false) != null)
            //        {
            //            RegistryKey BrowserPathKey = Registry.ClassesRoot.OpenSubKey(key, false);
            //            BrowserPath = BrowserPathKey.GetValue("").ToString().Split('"')[1];
            //        }
            //        else if (Registry.LocalMachine.OpenSubKey(@"\SOFTWARE\Classes\" + key, false) != null)
            //        {
            //            RegistryKey BrowserPathKey = Registry.ClassesRoot.OpenSubKey(@"\SOFTWARE\Classes\" + key, false);
            //            BrowserPath = BrowserPathKey.GetValue("").ToString().Split('"')[1];
            //        }
            //    }
            //    catch (Exception) { }

            //    if (string.IsNullOrEmpty(BrowserPath)) return;
                
            //    Process.Start(BrowserPath, string.Format("\"file:///{0}/HelpFiles/index.html\"", AppDir));
            //}
        }

        private void ShowOptoionsDialog()
        {
            MHEventSystem.SendTheRecMHEvent(this, new TheRecMHRecordingPreviewStopEventArgs());

            OptionsWindow optionsDialog = new OptionsWindow();
            optionsDialog.Owner = this;
            optionsDialog.ShowDialog();
            if (optionsDialog.DialogResult.HasValue && optionsDialog.DialogResult.Value)
            {
                if (this.DataContext != null && DataContext is IDisposable)
                {
                    ((IDisposable)this.DataContext).Dispose();
                }
                this.DataContext = new MHRecording();
            }

            MHEventSystem.SendTheRecMHEvent(this, new TheRecMHRecordingPreviewStartEventArgs());
        }

        private void Help_CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string AppDir = Directory.GetCurrentDirectory();
            string key = @"http\shell\open\command";
            string BrowserPath = null;

            try
            {
                if (Registry.ClassesRoot.OpenSubKey(key, false) != null)
                {
                    RegistryKey BrowserPathKey = Registry.ClassesRoot.OpenSubKey(key, false);
                    BrowserPath = BrowserPathKey.GetValue("").ToString().Split('"')[1];
                }
                else if (Registry.LocalMachine.OpenSubKey(@"\SOFTWARE\Classes\" + key, false) != null)
                {
                    RegistryKey BrowserPathKey = Registry.ClassesRoot.OpenSubKey(@"\SOFTWARE\Classes\" + key, false);
                    BrowserPath = BrowserPathKey.GetValue("").ToString().Split('"')[1];
                }
            }
            catch (Exception) { }

            if (string.IsNullOrEmpty(BrowserPath)) return;

            Process.Start(BrowserPath, string.Format("\"file:///{0}/HelpFiles/index.html\"", AppDir));
        }
    }
}
